import React, { useEffect } from "react";
import { Grid, Button, Typography } from "@material-ui/core";
import { InputField, FileUpload, FormWrapper, CommonTable } from "../reusables";
import { useStyles } from "../styles/FeildStyle";
import { useFormikContext } from "formik";
import "../styles/masters/conversation/ConversationSubList.scss";
import PublishIcon from "@material-ui/icons/Publish";
import AudiotrackIcon from "@material-ui/icons/Audiotrack";

function CommonSubForm(props) {
  const {
    Schema,
    FormCancel,
    COLUMNS,
    errorName,
    subFormHeaderText,
    formKey,
    getEditId,
    subFormEditIndex
  } = props;
  let Styles = useStyles();
  const { values, touched,setTouched,setErrors, setFieldValue, errors, submitForm } =
    useFormikContext();

  const subFormFun = async () => {
    await setFieldValue(formKey, true);
    await submitForm();
  };



  return (
    <div style={{ marginTop: 20 }}>
      <FormWrapper>
        <Grid container spacing={3}>
          <Grid iten xs={12}>
            <Typography>{subFormHeaderText}</Typography>
          </Grid>

          <Grid item xs={8}>
            <InputField
              name={Schema.feild1.name}
              label={Schema.feild1.label}
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
          <Grid item xs={4} >
            <FileUpload
              type="file"
              accept="image/*"
              hidden
              name={Schema.feild2.name}
              startIcon={<PublishIcon style={{ fontSize: 30 }} />}
              uploadFileIcon={<AudiotrackIcon style={{ fontSize: 30 }} />}
              btnText={Schema.feild2.BtnName}
              uploadFileKeys={Schema.feild2.uploadFileKeys}
            />
          </Grid>
          <Grid item xs={8}>
            <InputField
              name={Schema.feild3.name}
              label={Schema.feild3.label}
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
          <Grid item xs={4} className={Styles.fileUploadContainer}>
            <FileUpload
              type="file"
              accept="image/*"
              hidden
              name={Schema.feild4.name}
              startIcon={<PublishIcon style={{ fontSize: 30 }} />}
              uploadFileIcon={<AudiotrackIcon style={{ fontSize: 30 }} />}
              btnText={Schema.feild4.BtnName}
              uploadFileKeys={Schema.feild4.uploadFileKeys}
            />
          </Grid>

          <Grid item xs={12} className={Styles.subFormFooterContainer}>
            <Button variant="text" onClick={() => FormCancel("subForm")}>
              cancel
            </Button>
            <Button
              variant="text"
              style={{ color: "darkred" }}
              onClick={() => subFormFun()}
            >
              {subFormEditIndex === null ? "add" : "edit"}
              
            </Button>
          </Grid>
          {values[formKey] === false && touched[errorName] &&
            typeof errors[errorName] !== "undefined" && (
              <Grid
                item
                xs={12}
                style={{ display: "flex", justifyContent: "center" }}
              >
                <Typography style={{ color: "red" }}>
                  {errors[errorName]}
                </Typography>
              </Grid>
            )}
        </Grid>
        <Grid item xs={12}>
          <CommonTable
            columns={COLUMNS(getEditId)}
            data={values[errorName]}
            selection={[]}
            LoadingComponent={() => <div></div>}
            tableControlProps={{
              tableTitle: "",
              search: false,
              filter: false,
              addNew: false,
              download: false,
            }}
          />
        </Grid>
      </FormWrapper>
    </div>
  );
}

export default CommonSubForm;
