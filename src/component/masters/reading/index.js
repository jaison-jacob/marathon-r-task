import React from "react";
import { FormWrapper } from "../../reusables";
import { Grid, Typography } from "@material-ui/core";
import {
  InputField,
  RadioField,
  AutoCompleteField,
  SelectField,
  FileUpload,
} from "../../reusables";
import { useStyles } from "../../styles/FeildStyle";
import { useStylesFromReading } from "../../styles/masters/reading/ReadingStyle";
import PublishIcon from "@material-ui/icons/Publish";


function ReadingMainForm(props) {
  let Styles = useStyles();
  let readingStyle = useStylesFromReading();
  const {
    groups,
    subGroups,
    language,
    keywords,
    dropDownOnchange,
    radioOnchange,
    mainFormAudioFileUploadKeys,
  } = props;

  return (
    <div>
      <FormWrapper>
        <Grid container spacing={3}>
          <Grid item xs={3}>
            <InputField
              name="baseStoryName"
              label="Base story name"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
          <Grid item xs={5}>
            <FileUpload
              type="file"
              accept="image/*"
              hidden
              name="baseStoryImage"
              startIcon={<PublishIcon style={{ fontSize: 30 }} />}
              UploadBtnStyle={{
                marginTop: "0.4rem",
                fontSize: "0.7rem",
                fontWeight: 600,
              }}
              uploadTextStyle={{ fontSize: 10 }}
              btnText="Baner Image"
              uploadFileKeys={mainFormAudioFileUploadKeys}
            />
          </Grid>
          <Grid item xs={4} className={Styles.radioContainer}>
            <Typography className={readingStyle.radioLabelText}>
              Image Position
            </Typography>
            <div className={Styles.radioFieldContainer}>
              <RadioField
                labelClasses={Styles.radioHead}
                name="imagePosition"
                isLabelVisssible={{ display: "none" }}
                options={[
                  { id: 1, name: "Top" },
                  { id: 2, name: "Bottom" },
                ]}
                radioOnchange={radioOnchange}
                radioLabel={{
                  label: readingStyle.radioLabel,
                }}
                boxColor={{ color: "grey", fontSize: "1rem" }}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <InputField
              name="baseStoryText"
              label="About story"
              multiline={true}
              rows={6}
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
          <Grid item xs={12}>
            <AutoCompleteField
              options={keywords}
              style={{ outlined: Styles.chip }}
              chipVariant="outlined"
              textVariant="filled"
              label="Keywords"
              name="keywords"
            />
          </Grid>

          <Grid item xs={4}>
            <SelectField
              variant="filled"
              label="Base story Lang"
              selectLabelStyle={Styles.selectLabel}
              name="baseStoryLang"
              color="primary"
              selectOptionStyle={Styles.selectOption}
              options={language}
              required={true}
              dropDownOnchange={dropDownOnchange}
            />
          </Grid>
          <Grid item xs={8}></Grid>
          <Grid item xs={4}>
            <SelectField
              variant="filled"
              label="Group Name"
              selectLabelStyle={Styles.selectLabel}
              name="groupId"
              color="primary"
              selectOptionStyle={Styles.selectOption}
              options={groups}
              required={true}
              dropDownOnchange={dropDownOnchange}
            />
          </Grid>
          <Grid item xs={8}>
            <SelectField
              variant="filled"
              label="Select sub-group"
              selectLabelStyle={Styles.selectLabel}
              name="subGroupId"
              color="primary"
              selectOptionStyle={Styles.selectOption}
              options={subGroups}
              required={true}
              dropDownOnchange={dropDownOnchange}
            />
          </Grid>
        </Grid>
      </FormWrapper>
    </div>
  );
}

export default ReadingMainForm;
