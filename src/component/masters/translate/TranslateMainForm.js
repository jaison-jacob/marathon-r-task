import React from 'react'
import  {FormWrapper} from "../../reusables"
import { Grid } from "@material-ui/core";
import {InputField,AutoCompleteField,SelectField,FileUpload} from "../../reusables"
import {useStyles} from "../../styles/FeildStyle"
import PublishIcon from "@material-ui/icons/Publish";
import AudiotrackIcon from "@material-ui/icons/Audiotrack";


function TranslateMainForm(props) {
    let Styles = useStyles();
    const {groups,subGroups,dropDownOnchange,mainFormAudioFileUploadKeys} = props
    return (
      
        <div>
        <FormWrapper>
          <Grid container spacing={3}>
            <Grid item xs={4}>
            <InputField
            name="name"
            label="Translation name"
            fullWidth={true}
            variant="filled"
            color="primary"
            textStyle={Styles.textField}
          />
            </Grid>
            <Grid item xs={8}></Grid>
            <Grid item xs={3}>
            <InputField
            name="english"
            label="English"
            fullWidth={true}
            variant="filled"
            color="primary"
            textStyle={Styles.textField}
            disabled={true}
          />
            </Grid>
            <Grid item xs={5}>
            <InputField
            name="englishText"
            label="English language Text"
            fullWidth={true}
            variant="filled"
            color="primary"
            textStyle={Styles.textField}
            
          />
            </Grid>
            <Grid item xs={3}>
            <FileUpload
              type="file"
              accept="image/*"
              hidden
              name="englishAudioName"
              startIcon={<PublishIcon style={{ fontSize: 30 }} />}
              uploadFileIcon={<AudiotrackIcon style={{ fontSize: 30 }} />}
              btnText="Audio"
              uploadFileKeys={mainFormAudioFileUploadKeys}
            />
            </Grid>
            <Grid item xs={4}>
            <SelectField
              variant="filled"
              label="Group Name"
              selectLabelStyle={Styles.selectLabel}
              name="groupId"
              color="primary"
              selectOptionStyle={Styles.selectOption}
              options={groups}
              required={true}
              dropDownOnchange={dropDownOnchange}
            />
            
            </Grid>
            <Grid item xs={4}>
            <SelectField
              variant="filled"
              label="Select sub-group"
              selectLabelStyle={Styles.selectLabel}
              name="subGroupId"
              color="primary"
              selectOptionStyle={Styles.selectOption}
              options={subGroups}
              required={true}
              dropDownOnchange={dropDownOnchange}
            />
            
            </Grid>
            </Grid>
           
        </FormWrapper>
        </div>
    )
}

export default TranslateMainForm
