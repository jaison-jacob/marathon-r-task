import React, { useEffect } from "react";
import { Grid, Button, Typography } from "@material-ui/core";
import { InputField, FileUpload, FormWrapper, CommonTable,SelectField } from "../../reusables";
import { useStyles } from "../../styles/FeildStyle";
import { useFormikContext } from "formik";
import "../../styles/masters/conversation/ConversationSubList.scss";
import PublishIcon from "@material-ui/icons/Publish";
import AudiotrackIcon from "@material-ui/icons/Audiotrack";

function TranslateSubForm(props) {
  const {
    Schema,
    FormCancel,
    COLUMNS,
    errorName,
    subFormHeaderText,
    formKey,
    getEditId,
    subFormEditIndex,
    dropDownOnchange,
    SubFormUploadKeys,
    language
  } = props;
  let Styles = useStyles();
  const { values, touched,setTouched,setErrors, setFieldValue, errors, submitForm } =
    useFormikContext();

  const subFormFun = async () => {
    await setFieldValue(formKey, true);
    await submitForm();
  };

console.log(errors)
  return (
    <div style={{ marginTop: 20 }}>
      <FormWrapper>
        <Grid container spacing={3}>
          <Grid iten xs={12}>
            <Typography>{subFormHeaderText}</Typography>
          </Grid>
        <Grid item xs={3}>
        <SelectField
              variant="filled"
              label="Language"
              selectLabelStyle={Styles.selectLabel}
              name="translateData.sourceLangId"
              color="primary"
              selectOptionStyle={Styles.selectOption}
              options={language}
              required={true}
              dropDownOnchange={dropDownOnchange}
            />
        </Grid>

          <Grid item xs={5}>
            <InputField
              name="translateData.sourceText"
              label="Target Language Text"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
          <Grid item xs={4} >
            <FileUpload
              type="file"
              accept="image/*"
              hidden
              name="translateData.sourceAudioName"
              startIcon={<PublishIcon style={{ fontSize: 30 }} />}
              uploadFileIcon={<AudiotrackIcon style={{ fontSize: 30 }} />}
              btnText="Audio"
              uploadFileKeys={SubFormUploadKeys}
            />
          </Grid>
          

          <Grid item xs={12} className={Styles.subFormFooterContainer}>
            <Button variant="text" onClick={() => FormCancel("subForm")}>
              cancel
            </Button>
            <Button
              variant="text"
              style={{ color: "darkred" }}
              onClick={() => subFormFun()}
            >
              {subFormEditIndex === null ? "add" : "edit"}
              
            </Button>
          </Grid>
          {values[formKey] === false && touched[errorName] &&
            typeof errors[errorName] !== "undefined" && (
              <Grid
                item
                xs={12}
                style={{ display: "flex", justifyContent: "center" }}
              >
                <Typography style={{ color: "red" }}>
                  {errors[errorName]}
                </Typography>
              </Grid>
            )}
        </Grid>
        <Grid item xs={12}>
          <CommonTable
            columns={COLUMNS(getEditId,language)}
            data={values["translationDetails"]}
            selection={[]}
            LoadingComponent={() => <div></div>}
            tableControlProps={{
              tableTitle: "",
              search: false,
              filter: false,
              addNew: false,
              download: false,
            }}
          />
        </Grid>
      </FormWrapper>
    </div>
  );
}

export default TranslateSubForm;
