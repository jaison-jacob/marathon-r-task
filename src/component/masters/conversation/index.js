import React from 'react'
import  {FormWrapper} from "../../reusables"
import { Grid } from "@material-ui/core";
import {InputField,SelectField,FileUpload} from "../../reusables"
import {useStyles} from "../../styles/FeildStyle"
import PublishIcon from "@material-ui/icons/Publish";
import AudiotrackIcon from "@material-ui/icons/Audiotrack";


function ConversationMainForm(props) {
    let Styles = useStyles();
    const {groups,subGroups,dropDownOnchange,mainFormAudioFileUploadKeys} = props
    return (
     
        <div>
        <FormWrapper>
          <Grid container spacing={3}>
            <Grid item xs={12}>
            <InputField
            name="name"
            label="conversation name"
            fullWidth={true}
            variant="filled"
            color="primary"
            textStyle={Styles.textField}
          />
            </Grid>
            <Grid item xs={4}>
            <SelectField
              variant="filled"
              label="Group Name"
              selectLabelStyle={Styles.selectLabel}
              name="groupId"
              color="primary"
              selectOptionStyle={Styles.selectOption}
              options={groups}
              required={true}
              dropDownOnchange={dropDownOnchange}
            />
            
            </Grid>
            <Grid item xs={8}>
            <SelectField
              variant="filled"
              label="Select SubGroup Name"
              selectLabelStyle={Styles.selectLabel}
              name="subGroupId"
              color="primary"
              selectOptionStyle={Styles.selectOption}
              options={subGroups}
              required={true}
              dropDownOnchange={dropDownOnchange}
            />
            </Grid>
           
            <Grid item xs={8}>
            <FileUpload
              type="file"
              accept="image/*"
              hidden
              name="audioName"
              startIcon={<PublishIcon style={{ fontSize: 30 }} />}
              uploadFileIcon={<AudiotrackIcon style={{ fontSize: 30 }} />}
              btnText="Audio Upload"
              uploadFileKeys={mainFormAudioFileUploadKeys}
            />
            </Grid>
            </Grid>
           
        </FormWrapper>
        </div>
    )
}

export default ConversationMainForm
