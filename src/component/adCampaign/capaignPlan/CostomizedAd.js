import React from "react";
import { FormWrapper } from "../../reusables";
import { Grid, Typography } from "@material-ui/core";
import { RadioField, FileUpload } from "../../reusables";
import { useStyles } from "../../styles/FeildStyle";
import PublishIcon from "@material-ui/icons/Publish";
import AudiotrackIcon from "@material-ui/icons/Audiotrack";

function CostomizedAd(props) {
  let Styles = useStyles();
  const { radioOnchange, fileType,FileUploadKeys } = props;

  return (
    <div style={{ marginTop: "2rem" }}>
      <FormWrapper>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Typography>Costomized Ad</Typography>
          </Grid>

          <Grid item xs={6} className={Styles.radioContainer}>
            <Typography className={Styles.radioLabelText}>
              Display Ad
            </Typography>
            <div className={Styles.radioFieldContainer}>
              <RadioField
                labelClasses={Styles.radioHead}
                name="countryId"
                isLabelVisssible={{ display: "none" }}
                options={[
                  { id: 1, name: "Yes" },
                  { id: 2, name: "No" },
                ]}
                radioOnchange={radioOnchange}
                radioLabel={{
                  label: Styles.radioLabel,
                }}
                boxColor={{ color: "blue", fontSize: "1.3rem" }}
              />
            </div>
          </Grid>
          <Grid item xs={1}></Grid>

          <Grid item xs={12} className={Styles.radioContainer}>
            <Typography className={Styles.radioLabelText}>
              Campaign Type
            </Typography>
            <div className={Styles.radioFieldContainer}>
              <RadioField
                labelClasses={Styles.radioHead}
                name="customCampaignType"
                isLabelVisssible={{ display: "none" }}
                options={fileType}
                radioOnchange={radioOnchange}
                radioLabel={{
                  label: Styles.radioLabel,
                }}
                boxColor={{ color: "blue", fontSize: "1.3rem" }}
              />
            </div>
          </Grid>

          <Grid item xs={12} className={Styles.fileUploadContainer}>
            <FileUpload
              type="file"
              accept="image/*"
              hidden
              name="fileName"
              endIcon={<PublishIcon style={{ fontSize: 30, color: "blue" }} />}
              uploadFileIcon={<AudiotrackIcon style={{ fontSize: 30 }} />}
              btnText="Upload File"
              btnStyle={{
                root: Styles.fileUploadBtnStyle,
                fullWidth: Styles.fileUploadSizeBtnStyle,
              }}
              btnContainerStyle={{ width: "20rem" }}
              
              uploadFileKeys={FileUploadKeys}
            />
          </Grid>
        </Grid>
      </FormWrapper>
    </div>
  );
}

export default CostomizedAd;
