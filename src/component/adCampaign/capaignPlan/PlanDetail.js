import React from "react";
import { FormWrapper } from "../../reusables";
import { Grid, Typography } from "@material-ui/core";
import {
  InputField,
  SelectField,
  AutoCompleteField,
  RadioField,
  DatePickerField,
} from "../../reusables";
import { useStyles } from "../../styles/FeildStyle";
import { useFormikContext } from "formik";

function PlanDetail(props) {
  let Styles = useStyles();
  const {
    radioOnchange,
    dropDownOnchange,
    companyType,
    country,
    state,
    district,
    language,
    gender,
    users,
  } = props;
  const { values } = useFormikContext();

  return (
    <div>
      <FormWrapper>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <SelectField
              variant="filled"
              label="Select company"
              selectLabelStyle={Styles.selectLabel}
              name="companyId"
              color="primary"
              selectOptionStyle={Styles.selectOption}
              options={companyType}
              required={true}
              dropDownOnchange={dropDownOnchange}
            />
          </Grid>

          <Grid item xs={12}>
            <InputField
              name="name"
              label="Campaign Name"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
              required={true}
            />
          </Grid>

          <Grid item xs={3}>
            <DatePickerField
              autoOk={true}
              variant="inline"
              inputVariant="filled"
              name="startDate"
              label="start date"
              format="MM/dd/yyyy"
              InputAdornmentProps={{ position: "end" }}
              // disablePast={true}
            />
          </Grid>
          <Grid item xs={3}>
            <DatePickerField
              autoOk={true}
              variant="inline"
              inputVariant="filled"
              name="endDate"
              label="end date"
              format="MM/dd/yyyy"
              InputAdornmentProps={{ position: "end" }}
            />
          </Grid>
          <Grid item xs={3}>
            <InputField
              name="startTime"
              label="Start Time"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
              type="time"
            />
          </Grid>
          <Grid item xs={3}>
            <InputField
              name="endTime"
              label="End Time"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
              type="time"
            />
          </Grid>
          <Grid item xs={6}>
            <SelectField
              variant="filled"
              label="Basic language"
              selectLabelStyle={Styles.selectLabel}
              name="languageId"
              color="primary"
              selectOptionStyle={Styles.selectOption}
              options={language}
              dropDownOnchange={dropDownOnchange}
            />
          </Grid>
          <Grid item xs={1}></Grid>
          <Grid item xs={6} className={Styles.radioContainer}>
            <Typography className={Styles.radioLabelText}>Location</Typography>
            <div className={Styles.radioFieldContainer}>
              <RadioField
                labelClasses={Styles.radioHead}
                name="countryId"
                isLabelVisssible={{ display: "none" }}
                options={country}
                radioOnchange={radioOnchange}
                radioLabel={{
                  label: Styles.radioLabel,
                }}
                boxColor={{ color: "blue", fontSize: "1.3rem" }}
              />
            </div>
          </Grid>
          <Grid item xs={1}></Grid>

          {values.countryId === 1 ? (
            <>
              <Grid item xs={6}>
                <SelectField
                  variant="filled"
                  label="State"
                  selectLabelStyle={Styles.selectLabel}
                  name="stateName"
                  color="primary"
                  selectOptionStyle={Styles.selectOption}
                  options={state}
                  dropDownOnchange={dropDownOnchange}
                />
              </Grid>
              <Grid item xs={6}>
                <SelectField
                  variant="filled"
                  label="District"
                  selectLabelStyle={Styles.selectLabel}
                  name="districtName"
                  color="primary"
                  selectOptionStyle={Styles.selectOption}
                  options={district}
                  required={true}
                  dropDownOnchange={dropDownOnchange}
                />
              </Grid>
            </>
          ) : (
            <>
              <Grid item xs={6}>
                <InputField
                  name="countryName"
                  label="countryName"
                  fullWidth={true}
                  variant="filled"
                  color="primary"
                  textStyle={Styles.textField}
                />
              </Grid>
              <Grid item xs={6}>
                <InputField
                  name="cityText"
                  label="city"
                  fullWidth={true}
                  variant="filled"
                  color="primary"
                  textStyle={Styles.textField}
                />
              </Grid>
            </>
          )}

          <Grid item xs={6} className={Styles.radioContainer}>
            <Typography className={Styles.radioLabelText}>Gender</Typography>
            <div className={Styles.radioFieldContainer}>
              <RadioField
                labelClasses={Styles.radioHead}
                name="genderId"
                isLabelVisssible={{ display: "none" }}
                options={gender}
                radioOnchange={radioOnchange}
                radioLabel={{
                  label: Styles.radioLabel,
                }}
                boxColor={{ color: "blue", fontSize: "1.3rem" }}
              />
            </div>
          </Grid>

          <Grid item xs={1}></Grid>

          <Grid item xs={6}>
            <AutoCompleteField
              options={users}
              style={{ outlined: Styles.chip }}
              chipVariant="outlined"
              textVariant="filled"
              label="User list (All)"
              name="users"
              typeMenuIte="firstName"
            />
          </Grid>
        </Grid>
      </FormWrapper>
    </div>
  );
}

export default PlanDetail;
