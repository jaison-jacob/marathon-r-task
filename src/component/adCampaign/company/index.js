import React,{useEffect} from "react";
import { FormWrapper } from "../../reusables";
import { Grid } from "@material-ui/core";
import { InputField, SelectField, AutoCompleteField } from "../../reusables";
import { useStyles } from "../../styles/FeildStyle";
import { FormikContext, useFormikContext } from "formik";

function CompanyForm(props) {
  let Styles = useStyles();
  const { getDistrictById,dropDownOnchange,setDistrict,companyType, country, state, district } = props;
  const { values } = useFormikContext();
// console.log(country)
// console.log(values)
  return (
    <div>
      <FormWrapper>
        <Grid container spacing={3}>
          
          <Grid item xs={12}>
            <InputField
              name="name"
              label="Company Name"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
              required={true}
              disabled={values.disabled}
            />
          </Grid>
          <Grid item xs={12}>
            <SelectField
              variant="filled"
              label="Company Type"
              selectLabelStyle={Styles.selectLabel}
              name="companyTypeId"
              color="primary"
              selectOptionStyle={Styles.selectOption}
              options={companyType}
              required={true}
              dropDownOnchange={dropDownOnchange}
            />
          </Grid>
          <Grid item xs={12}>
            <InputField
              name="companyAddress"
              label="Company Address"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
              
            />
          </Grid>
          <Grid item xs={6}>
            <SelectField
              variant="filled"
              label="Country"
              selectLabelStyle={Styles.selectLabel}
              name="countryId"
              color="primary"
              selectOptionStyle={Styles.selectOption}
              options={country}
              dropDownOnchange={dropDownOnchange}
            />
          </Grid>
          {values.countryId === 1 ? (
            <>
              <Grid item xs={6}>
                <SelectField
                  variant="filled"
                  label="State"
                  selectLabelStyle={Styles.selectLabel}
                  name="stateName"
                  color="primary"
                  selectOptionStyle={Styles.selectOption}
                  options={state}
                  dropDownOnchange={dropDownOnchange}
                />
              </Grid>
              <Grid item xs={6}>
                <SelectField
                  variant="filled"
                  label="District"
                  selectLabelStyle={Styles.selectLabel}
                  name="districtName"
                  color="primary"
                  selectOptionStyle={Styles.selectOption}
                  options={district}
                  required={true}
                  dropDownOnchange={dropDownOnchange}
                />
              </Grid>
            </>
          ) : (
            <Grid item xs={6}>
              <InputField
                name="cityText"
                label="city"
                fullWidth={true}
                variant="filled"
                color="primary"
                textStyle={Styles.textField}
              />
            </Grid>
          )}
          </Grid>
          <Grid container spacing={3}>
          <Grid item xs={6}>
            <InputField
              name="userName"
              label="Contact Name"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
          <Grid item xs={6}>
            <InputField
              name="userDesignation"
              label="Contact Designation"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
          <Grid item xs={6}>
            <InputField
              name="userEmail"
              type="email"
              label="Contact email"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
          <Grid item xs={6}>
            <InputField
              name="userMobile"
              type="number"
              label="Contact number"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
          <Grid item xs={6}>
            <InputField
              name="pan"
              label="Pan number"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
          <Grid item xs={6}>
            <InputField
              name="gst"
              label="GST"
              fullWidth={true}
              variant="filled"
              color="primary"
              textStyle={Styles.textField}
            />
          </Grid>
          </Grid>
      </FormWrapper>
    </div>
  );
}

export default CompanyForm;
