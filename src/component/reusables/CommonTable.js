import React from "react";
import { Table } from "react_custom_table";
import { ArrowDropUp, ArrowDropDown, Bathtub } from "@material-ui/icons";

function CommonTable(props) {
  const {a,columns,selectable,onChangePageSize,onPageNumberChange,totalCount,pageSize,currentPage,showPagination,shouldHideSelect,selectedBackground,onSelect,sortIconTop,getEditId,sortIconBottom,sortIconTopBot,data,selection,LoadingComponent,tableControlProps,emptyDataMessage} = props
  console.log(pageSize,currentPage)
  const paginationProps = !showPagination
    ? {}
    : {
        totalCount: totalCount,
        pageSize: pageSize,
        currentPage: currentPage,
        onPageNumberChange: (pageNumber) => {
          onPageNumberChange(pageNumber);
        },
        onChangePageSize: (size) => {
          onChangePageSize(size);
        },
      };
 console.log(paginationProps)
  return (
    <div>
      {data.length > 0 && (
        <Table
          columns={columns}
          data={data}
          selection={selection}
          onSelect={onSelect}
          shouldHideSelect={shouldHideSelect}
          selectedBackground={selectedBackground}
          LoadingComponent={() => LoadingComponent}
          tableControlProps={{...tableControlProps}}
          emptyDataMessage={{
            ...emptyDataMessage
          }}
          sortIconTop={sortIconTop}
          sortIconBottom={sortIconBottom}
          sortIconTopBot={sortIconTopBot}
          onSort={true}
          selectable={selectable}
          showPagination={showPagination}
          paginationProps={paginationProps}
        />
      )}
    </div>
  );
}

export {CommonTable};
