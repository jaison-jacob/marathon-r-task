import React from "react";
import PropTypes from "prop-types";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  root: {
    display: "grid",
    placeItems: "center",
    position: "absolute",
    top: 0,
    width: "100vw",
    height: "100vh",
    zIndex: "9999",
    backgroundColor: "#ffffff77",
  },
}));

export function CircularStatic({ isloading }) {
  const styles = useStyles();

  return (
    <>
    {
      (isloading)?(
        <div className={styles.root}>
      <CircularProgress size={80} />
    </div>
      ):null
    }
    </>
  );
}
