import React, { useEffect, useRef, useState } from "react";
import { useStyles } from "../styles/FormFooter";
import { ButtonWrapper } from "../reusables";
import { useFormikContext } from "formik";
// import { initialValue,a } from "../../constants/induvidualUser";
import { Modal, Alert } from "../reusables";
// import {getInidividualUserBYId,getLanguages,getKnowingViaProduct} from "../../api/Api"
import { useHistory } from "react-router-dom";


function FormFooter(props) {
  const styles = useStyles();
  const history = useHistory();
  const {values,touched,errors,setErrors,setFieldValue,submitForm} = useFormikContext();
  const {submitType,formKey,FormCancel,updateText,formik} = props

  const a = async() =>{
    console.log(formKey)
    typeof formKey !== "undefined" && (await setFieldValue(formKey,false))
   await submitForm()
  }

  return (
    <div className={styles.footerContainer}>
      <div className={styles.buttonContainer}>
        <ButtonWrapper
          text="Cancel"
          btnStyle={styles.cancelBtn}
          size="medium"
          variant="outlined"
          BtnAction={() => FormCancel("main")}
        />
        <ButtonWrapper
          text={updateText ? "Update":"save"}
          // type="submit"
          btnStyle={styles.saveBtn}
          size="medium"
          variant="contained"
          BtnAction={() => a()}
          type={submitType}
        />

        
      </div>
    </div>
  );
}

export {FormFooter};
