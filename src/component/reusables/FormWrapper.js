import React from 'react'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    formContainer:{
        // width:"100%",
        // color:"#FFFFFF",
        backgroundColor:"#F1F1F1",
        marginLeft:theme.spacing(5),
        padding:"5%",
        boxShadow:"0px 2px 4px #0000001A",
        zIndex:100,
        opacity:1,
    }
}))

function FormWrapper(props) {
    const classes = useStyles()
    return (
        <div className={classes.formContainer}>
            {props.children}
        </div>
    )
}

export {FormWrapper}
