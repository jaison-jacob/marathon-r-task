import { Grid, Typography } from "@material-ui/core";
import React from "react";
import { useHistory } from "react-router-dom";
import { ButtonWrapper } from "../reusables";
import { useStyles } from "../styles/Nodata";
import { EmsPath } from "../../router/EmsPath";

function Nodata(props) {
  const { text,btnText,path } = props;
  const Styles = useStyles();
  let history = useHistory();
  const a = () => {
    history.push(path);
  };
  return (
    <Grid container style={{padding:"30px 20px"}}>
      <Grid
        item
        xs={12}
        
      >
          <div style={{display: "flex",alignItems:"center",justifyContent:"space-between"  }}>
        <Typography className={Styles.headText}>{text}</Typography>
        <ButtonWrapper
          text={btnText}
          // type="submit"
          btnStyle={Styles.btn}
          size="medium"
          variant="contained"
          BtnAction={() => a()}
        />
        </div>
      </Grid>
      <Grid item xs={12} style={{marginTop:50}}>
        <Typography className={Styles.content}>No data found</Typography>
      </Grid>
    </Grid>
  );
}

export default Nodata;
