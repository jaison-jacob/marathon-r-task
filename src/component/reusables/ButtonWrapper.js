import { Button } from '@material-ui/core'
import React from 'react'

function ButtonWrapper(props) {
    const {text,btnStyle,variant,size,type,disabled,BtnAction,...otherProps} = props

    return (
        <div>
            <Button className={btnStyle} type={type}  disabled={disabled} size={size}variant={variant}{...otherProps} onClick={BtnAction}>
                {text}
            </Button>
        </div>
    )
}

export {ButtonWrapper}
