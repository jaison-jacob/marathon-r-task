import React from "react";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import { useHistory } from "react-router-dom";
import {EmsPath} from "../../router/EmsPath"

const useStyles = makeStyles({
  root: {
    width: 230,
    
  },
  btnStyle:{
    padding:0,
    
  }
});



const ITEM_HEIGHT = 48;

function ProfilePopUp(props) {
  let history = useHistory();
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const {options} = props;

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const l = async (index, type) => {
    // let state = { id: index, status: type };
    props.editMainListData(index,type)
    await history.push(EmsPath.MASTERSCONVERSATIONFORM);
  };
console.log(props.id)
  return (
    <div>
      <IconButton
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        onClick={handleClick}
        style={{marginLeft:"100%"}}
        classes={{root:classes.btnStyle}}
      >
        <MoreHorizIcon />
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            maxHeight: ITEM_HEIGHT * 4.5,
            width: "180px",
            fontSize: 2,
          },
        }}
      >
        {options.map((option) => (
          <MenuItem
            key={props.index}
            selected={option === "Pyxis"}
            // onClick={handleClose}
            className="profilePopupIteme"
            onClick={() => props.fun(props.id,option)}
          >
            {option}
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
}



export default ProfilePopUp;
