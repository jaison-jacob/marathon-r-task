import React from "react";
import { IconButton, Typography } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { useStyles } from "../styles/FormHeader";
import { useHistory } from "react-router-dom";

function FormHeader(props) {
  const styles = useStyles();
  let history = useHistory();
  const {path,name} = props;
  return (
    <div>
      <div className={styles.formHeader}>
        <IconButton
          aria-label="delete"
          className={styles.formHeaderIcon}
          size="medium"
          onClick={() => history.push(path, {})}
        >
          <ArrowBackIcon fontSize="inherit" />
        </IconButton>
        <Typography className={styles.formHeaderText}>
          {name}
        </Typography>
      </div>
    </div>
  );
}

export {FormHeader};