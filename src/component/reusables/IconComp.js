import React from "react";
import { Avatar, Button, IconButton, Typography } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from '@material-ui/icons/Delete';

function IconComp(props) {
  const {deleteIcon,getEditId} = props;
  const editAction = (id,type) => {
    getEditId(id,type);
  };
  return (
    <div style={{display:"flex",justifyContent:"space-between"}}>
      <IconButton style={{padding:0}} onClick={() => editAction(props.id,"edit")}>
        <EditIcon />
      </IconButton>
      {
        deleteIcon === true && (
<IconButton style={{padding:0}} onClick={() => editAction(props.id,"delete")}>
        <DeleteIcon />
      </IconButton>
        )
      }
      
    </div>
  );
}



export default IconComp;
