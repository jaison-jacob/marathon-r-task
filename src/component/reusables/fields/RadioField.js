import React from "react";
import {
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
  Typography,
} from "@material-ui/core";
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import {useField, useFormikContext} from "formik";
import { RadioButtonChecked } from "@material-ui/icons";

function RadioField(props) {

  const {name,radioOnchange,labelClasses,isLabelVisssible,radioLabel,label,boxColor} = props
  const {setFieldValue,values} = useFormikContext()

  const [field,mata] = useField(name)

  const handleChange = (e) => {
  
    radioOnchange(e,name,values,setFieldValue)
  }

const configTextField = {
  ...field,
  onChange:handleChange
}

if(mata&& mata.touched && mata.error){
  configTextField.error = true;
  configTextField.helperText = mata.error;
}


  return (
    <div >
      {/* change option array to array of object with id,name , change optiion to options */}
      {/* <Typography className={props.labelClasses}></Typography> */}
      <FormControl component="fieldset" style={{marginTop:8}}>
        <FormLabel component="legend" className={labelClasses} focused={false} style={isLabelVisssible}>
        {label}
        </FormLabel>
        <RadioGroup
        name={name}
          {...configTextField}
          row
        >
          {props.options.map((e, i) => (
            <FormControlLabel
              key={i}
              value={e.id}
              control={<Radio size="large"  icon={<RadioButtonUncheckedIcon style={boxColor}/>} checkedIcon={<RadioButtonChecked style={boxColor}/>} color={props.color}/>}
              label={e.name || ""}
              classes={props.radioLabel}
              disabled={values.disabled}
            />
          ))}
        </RadioGroup>
      </FormControl>
    </div>
  );
}

export {RadioField};
