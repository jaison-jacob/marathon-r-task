import React from "react";
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText,
} from "@material-ui/core";
import {useField,useFormikContext} from "formik"

function SelectField(props) {

  const {name,typeMenuIte,dropDownOnchange,required,label,selectLabelStyle,options,selectOptionStyle,textStyle,...otherProps} = props
const [field,mata] = useField(name)

const {values,setFieldValue} = useFormikContext()

const handleOnchange =(e)=>{
  dropDownOnchange(e,name,values,setFieldValue)
}

const configSelectField = {
  ...field,
  ...otherProps,
  onChange:handleOnchange
}

if(mata&& mata.touched && mata.error){
  configSelectField.error = true;
  configSelectField.helperText = mata.error;
}
// console.log(field.value)
// console.log("touch ",mata.touched)
// console.log("value ",(field.value === null))
// console.log(mata.touched ||  field.value !== null)
  return (
    <div>
      <FormControl
        variant="filled"
        fullWidth
        error={configSelectField.error }
        disabled={values.disabled}
        required={required}
      >
        <InputLabel shrink={mata.touched ||  field.value !== null} className={selectLabelStyle}>
          {label}
        </InputLabel>
        <Select
          {...configSelectField}
          className={selectOptionStyle}
          value={field.value}
        >
          {/* change value e.name to e.id, change optiion to options */}
          {options.map((e, i) => (
            <MenuItem value={e.id} key={i}>
             {typeMenuIte ? e[typeMenuIte] : e.name}
            </MenuItem>
          ))}
        </Select>
        <FormHelperText style={{color:"red"}}>{configSelectField.helperText}</FormHelperText>
      </FormControl>
    </div>
  );
}

export {SelectField};
