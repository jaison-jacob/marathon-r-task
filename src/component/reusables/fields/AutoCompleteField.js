import React from "react";
import { Chip, TextField } from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import {useFormikContext,useField} from "formik"

function AutoCompleteField(props) {

  const {name,typeMenuIte,options,style,chipVariant,textVariant,...otherProps} = props
  const {setFieldValue,values,errors} = useFormikContext()

  const [field,mata] = useField(name)

  const handleChange = (e) => {
  // console.log(name,e)
    setFieldValue(name,e)
  }

const configAutoCompleteField = {
  ...field,
  ...otherProps, 
}
// console.log(values)
if(mata&& mata.touched && mata.error){
  configAutoCompleteField.error = true;
  configAutoCompleteField.helperText = mata.error;
}

  return (
    <div>
      <Autocomplete
        multiple
        id="tags-filled"
        options={options}
        getOptionLabel={(option) => typeMenuIte? option[typeMenuIte]: option.name}
        onChange={(e, newValue, l) => {
        handleChange(newValue);
        }}
        disabled={values.disabled}
        renderTags={(value, getTagProps) =>
          value.map((option, index) => (
            <Chip
              key={index}
              classes={style}
              variant={chipVariant || "outlined"}
              label={ typeMenuIte? option[typeMenuIte]: option.name}
              {...getTagProps({ index })}
            />
          ))
        }
        renderInput={(params) => (
          <TextField
            {...params}
            variant={textVariant || "filled"}
            {...configAutoCompleteField}
          />
        )}
      />
    </div>
  );
}

export { AutoCompleteField };
