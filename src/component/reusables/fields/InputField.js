import React from "react";
import { TextField } from "@material-ui/core";
import {useField, useFormikContext} from "formik";

function InputField(props) {

const {name,multiline,rows,disabled,textStyle,...otherProps} = props

const [field,mata] = useField(name)
const {values,errors} = useFormikContext()
const configTextField = {
  ...field,
  ...otherProps,
}
if(mata&& mata.touched && mata.error){
  configTextField.error = true;
  configTextField.helperText = mata.error;
}
  return (
    <div>
      <TextField
        id="filled-basic"
        {...configTextField}
        inputProps={{
          className: textStyle,
        }}
        
        // InputLabelProps={{
        //   shrink:mata.touched ||typeof field.value !== "undefined",
        // }}
        disabled={values.disabled || disabled}
        multiline={multiline}
        rows={rows}
        // fullWidth
        // error={Boolean(props.errorText)}
        // helperText={props.errorText}
      />
    </div>
  );
}

export {InputField};
