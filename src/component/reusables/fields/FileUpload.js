import React from "react";
import { useField, useFormikContext } from "formik";
import { Grid, Button, Typography } from "@material-ui/core";
import { s3FileUpload } from "../../../utils/FileUpload";

function FileUpload(props) {
  const {
    name,
    uploadFileIcon,
    btnText,
    startIcon,
    endIcon,
    textStyle,
    btnStyle,
    btnContainerStyle,
    uploadFileKeys,
    UploadBtnStyle,
    uploadTextStyle,
    ...otherProps
  } = props;

  const { values, setFieldValue, setErrors } = useFormikContext();
  const [field, mata] = useField(name);
  const change = async (e) => {
    let uploadedFile = e.target.files[0];
    let result = await s3FileUpload(e.target.files[0], "jaison-bukket");

    setFieldValue(uploadFileKeys.Name, uploadedFile.name);
    setFieldValue(uploadFileKeys.awsName, result.location);
    setFieldValue(uploadFileKeys.Path, result.key);
    if (typeof uploadFileKeys.Type !== "undefined") {
      setFieldValue(uploadFileKeys.Type, uploadedFile.type);
    }
  };
  const configTextField = {
    ...otherProps,
    onChange: change,
  };
  if (mata && mata.touched && mata.error) {
    configTextField.error = true;
    configTextField.helperText = mata.error;
  }
  return (
    <div>
      <div style={{ display: "flex" }}>
        <div style={btnContainerStyle}>
          <Button
            variant="contained"
            startIcon={startIcon}
            endIcon={endIcon}
            component="label"
            fullWidth
            onclick={(e) => console.log(e.target.value)}
            classes={btnStyle}
            disabled={values.disabled}
            style={UploadBtnStyle || { marginTop: "0.4rem" }}
          >
            {btnText}
            <input {...configTextField} />
          </Button>
        </div>
        {field.value && (
          <div style={{ marginLeft: 10, marginTop: "1rem", display: "flex" }}>
            {uploadFileIcon}
            <Typography style={uploadTextStyle || { fontSize: 14 }}>
              {field.value}
            </Typography>
          </div>
        )}
      </div>

      <div>
        {!field.value && configTextField.helperText && (
          <p style={{ color: "red" }}>{configTextField.helperText}</p>
        )}
      </div>
    </div>
  );
}

export { FileUpload };
