import React from "react";
import {
  FormControl,
  FormLabel,
  FormGroup,
  FormControlLabel,
  Checkbox,
  FormHelperText,
} from "@material-ui/core";
import {useFormikContext,useField} from "formik"
import {CheckBoxOutlineBlank} from '@material-ui/icons';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
// import {checkedState} from "../../../constants/induvidualUser"


function SingleCheckBoxFeild(props) {

  const {name,checkBoxHead,boxColor,label,options,checkLabel,...otherProps} = props
  const {setFieldValue,values} = useFormikContext()

  const [field,mata] = useField(name)

  // const handleChange = (e) => {
  //   if(e.target.checked){
  //       setFieldValue(name,1)
  //   }else{
  //       setFieldValue(name,2)
  //   }
  // }

const configAutoCompleteField = {
  ...field,
  ...otherProps,
  // onChange:handleChange
}

if(mata&& mata.touched && mata.error){
  configAutoCompleteField.error = true;
  configAutoCompleteField.helperText = mata.error;
}

  return (
    // change the hard coded label to dynamic props label,  change the value to e.id
    <div>
      <FormControl component="fieldset" {...configAutoCompleteField}>
        {/* <FormLabel component="legend" focused={false} classes={checkBoxHead}>
          {label}
        </FormLabel> */}
        <FormGroup row>
         
            <FormControlLabel
            
              control={
                <Checkbox
                  checked={field.value === 1}
                  icon={<CheckBoxOutlineBlank style={boxColor}/>}
                   checkedIcon={<CheckBoxIcon  style={boxColor}/>}
                  size="small"
                  disabled={values.disabled}
                />
              
              }
              label= {label}
              classes={checkLabel}
              
            />

        </FormGroup>
      </FormControl>
    </div>
  );
}

export {SingleCheckBoxFeild};
