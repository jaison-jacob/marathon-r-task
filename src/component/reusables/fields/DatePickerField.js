import React from "react";
import {
  DatePicker,
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns"; // choose your lib
import {useFormikContext,useField} from "formik"

function DatePickerField(props) {

  const {name,...otherProps} = props
  const {setFieldValue,values} = useFormikContext()

  const [field,mata] = useField(name)

  const handleChange = (e) => {
   
    setFieldValue(name,e)
  }

const configAutoCompleteField = {
  ...field,
  ...otherProps,
  onChange:handleChange
}

if(mata&& mata.touched && mata.error){
  configAutoCompleteField.error = true;
  configAutoCompleteField.helperText = mata.error;
}



  return (
    // utils={DateFnsUtils}
    <div>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardDatePicker
          {...configAutoCompleteField}
          disabled={values.disabled}
        />
      </MuiPickersUtilsProvider>
    </div>
  );
}

export {DatePickerField};
