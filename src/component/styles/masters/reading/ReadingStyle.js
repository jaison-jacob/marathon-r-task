import { makeStyles } from "@material-ui/core";

export const useStylesFromReading = makeStyles((theme) => ({
    
    radioLabelText:{
        color:"grey",
        marginTop:"0.6rem",
        fontWeight:600,
        fontSize:"0.9rem"
    },
    radioLabel: {
        color: "#646464",
        fontSize: 14,
        fontFamily: "Roboto",
        textAlign: "left",
        opacity: 1,
        marginRight:"0rem",
        fontWeight:600
      },

}));
