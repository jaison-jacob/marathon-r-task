import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  headerContainer:{

  },
    title: {
      width: "60%",
      display: "flex",
      justifyContent: "flex-start",
      alignItems: "center",
      marginLeft: theme.spacing(8),
    },
    toolbar: {
      backgroundColor: "#2F5CC6",
      height: 64,
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
    },
    toolbarGutters: {
      paddingLeft: 0,
      paddingRight: 10,
    },
    profileContainer: {
      width: "11.05%",
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
    },
    profileText: {
      fontSize: 14,
      color: "#FFFFFF",
      fontFamily: "Rubik",
    },
    profileMenu: {
      marginTop: 10,
      fontFamily: "Dosis",
      fontSize: 14,
      textAlign: "left",
    },
    menuItemText: {
      fontSize: 14,
      fontFamily: "Dosis",
      color: "#0000008A",
    },
    navlink: {
      height: 64,
      marginLeft: 20,
      color: "#FFFFFFA3",
      fontSize: 15,
      textAlign: "center",
      textTransform: "capitalize",
      fontFamily: "Rubik",
      fontWeight: 100,
    },
    navlinkActive: {
      // backgroundColor: "#0B294D",
      color: "#FFFFFF",
      borderImage:
        "linear-gradient(to left, #2F5CC6 20%,#FFFFFF 15%,#FFFFFF 80%, #2F5CC6 25%)",
      borderBottom: "3px solid #FFFFFF",
      borderImageSlice: 1,
    },
  }));