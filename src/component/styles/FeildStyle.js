import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
    textField: {
        fontSize: 16,
        color: "#909090",
        fontFamily: "Rubik",
      },
      radioLabel: {
        color: "#646464",
        fontSize: 16,
        fontFamily: "Roboto",
        textAlign: "left",
        opacity: 1,
        marginLeft:"0.7rem",
        marginRight:"1rem",
        fontWeight:600
      },

      radioContainer:{
        display:"flex",
        alignItems:"center"
      },
      radioLabelText:{
        marginTop:"0.6rem",
        fontWeight:600,
        color:"blue"
      },
      radioFieldContainer:{
        marginLeft:"1.5rem"
      },
      radioHead: {
        color: "#909090",
        fontSize: 16,
        fontFamily: "Rubik",
        textAlign: "left",
        marginBottom:theme.spacing(2)
      },
      selectLabel: {
        fontSize: 16,
        color: "#909090",
        fontFamily: "Rubik",
        fontWeight: 200,
      },
      selectOption: {
        fontSize: 16,
        color: "#909090",
        fontFamily: "Rubik",
      },
      chip: {
        backgroundColor: "#C1B8B8",
        borderRadius: 16,
        fontSize: 12,
        color: "#000000DE",
        fontFamily: "Roboto",
      },
      checkLabel: {
        color: "#000000DE",
        fontSize: 14,
        fontFamily: "Rubik",
        textAlign: "left",
        opacity: 1,
      },
      sendEmail:{
        background:"#0000000A 0% 0% no-repeat padding-box",
      borderRadius:2,
      opacity:1,
      color:"#0000008A",
      fontFamily:"Rubik",
      fontSize:16,
      border:"none",
      textTransform:"capitalize"
      },
      subFormFooterContainer:{
        display:"flex",
        justifyContent:"flex-end",
        alignItems:"center"
      },
      fileUploadContainer:{
        // display:"flex",
        // alignItems:"center"
      },
      fileUploadBtnStyle:{
        justifyContent:"space-between",
        textTransform:"capitalize"
      },
      fileUploadSizeBtnStyle:{
        height:"4rem"
      }
}));
