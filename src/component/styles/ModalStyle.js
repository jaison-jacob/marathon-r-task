import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  btnColor: {
    color: "#909090",
    backgroundColor: "#DBEDFF",
  },
  dialogStyle: {
    width: 712,
  },
  dialogTittleContainer: {
    backgroundColor: "#2C56C6",
  },
  dialogTittle: {
    color: "#FFFFFF",
    fontFamily: "Rubik",
    fontSize: 20,
    fontWeight: 600,
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  btnStyle: {
    height: 42,
    width: "30%",
    fontFamily: "Roboto",
    fontSize: 16,
    letterSpacing: 0,
    textTransform: "capitalize",
    border: "1px solid #B4D9FF",
    "&:hover": {
      backgroundColor: "#DBEDFF",
    },
  },
}));
