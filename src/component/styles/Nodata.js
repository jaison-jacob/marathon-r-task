import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
    btn:{
        backgroundColor:"green",
        color:"white",
        height:50,
        minWidth:100
    },
    headText:{
        fontSize:20,
        fontWeight:700,
    },
    content:{
        display:"flex",
        justifyContent:"center",
        alignItems:"center"
    }
}));