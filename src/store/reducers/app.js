import {ISLOADING} from "../types/app";

const initialState = {
    isLoading:false
}

export default function reducer(state=initialState,action){
    const {type,payload} = action;
    switch (type){
        case ISLOADING:
            return {...state, isLoading:payload};
        default:
            return state    
    }
}