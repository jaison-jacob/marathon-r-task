import {ISLOADING} from "../types/app"

const appLoading = (status) => {
    return {type:ISLOADING,payload:status}
}

export {appLoading}