import { Avatar, Button, IconButton, Typography } from "@material-ui/core";
import AudiotrackIcon from "@material-ui/icons/Audiotrack";
import EditIcon from '@material-ui/icons/Edit';
// import IconComp from "../component/reusables/IconComp"
import ProfilePopUp from "../../component/reusables/ProfilePopUp"

const options = ["Edit Company","Delete Company"];
export const CompanyTableSchema = (props) =>

{
  console.log(props)
  return(
    [
      {
        Header: "Company Name",
        accessor: "name",
        width: 250,
        minWidth: 200,
        maxWidth: 300,
        color: "#0000008A",
      },
      {
        Header: "Company Type",
        accessor: "companyType.name",
        width: 250,
        minWidth: 200,
        maxWidth: 300,
        color: "green",
      },
      {
        Header: "Contact Name",
        accessor: "userName",
        width: 250,
        minWidth: 200,
        maxWidth: 300,
        color: "#000000B8",
      },
      {
        Header: "Contact Number",
        accessor: "userMobile",
        width: 250,
        minWidth: 200,
        maxWidth: 300,
        color: "#000000B8",
      },
      {
        Header: "Contact Email",
        accessor: "userEmail",
        width: 250,
        minWidth: 200,
        maxWidth: 300,
        color: "#000000B8",
      },
      {
       
        width: 250,
        minWidth: 200,
        maxWidth: 300,
        color: "#000000B8",
        Cell: (orgin) => {
          console.log(orgin)
          return(
           
                <div style={{display:"flex",justifyContent:"center"}}>
                <ProfilePopUp options={options} fun={props} id={orgin.original.id}/>
                </div>
          )
        }
      },
    ]
  )
}