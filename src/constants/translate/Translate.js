export const  translateState ={
	"name": "",
	"englishText": "",
	"englishAudio": "",
	"englishAudioName": "",
	"englishAudioType": "",
	"englishAudioAwsName": "",
	"groupId": null,
  subGroupId:null,
	"translationDetails": [],
  translateData:{
    "sourceLangId": null,
    "sourceText": "",
    "sourceAudio": "",
    "sourceAudioName": "",
    "sourceAwsAudioName": "",
    "sourceAudioType": ""
  },
  subFormActive:false
}

export const translateEnglishFileUploadKeys ={
  Path:"englishAudio",
  Name:"englishAudioName",
  awsName:"englishAudioAwsName",
  audioType:"englishAudioType",
}

export const translateTargetLanguageUploadKeys = {
  Path:"translateData.sourceAudio",
  Name:"translateData.sourceAudioName",
  awsName:"translateData.sourceAwsAudioName",
  Type:"translateData.sourceAudioType",
}
