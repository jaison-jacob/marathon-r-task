import { Avatar, Button, IconButton, Typography } from "@material-ui/core";
import AudiotrackIcon from "@material-ui/icons/Audiotrack";
import EditIcon from "@material-ui/icons/Edit";
import {} from "../../";
import IconComp from "../../component/reusables/IconComp";
import ProfilePopUp from "../../component/reusables/ProfilePopUp";

export const translateSubListColomn = (getEditId, language) => [
  {
    Header: "Target Language",
    accessor: "sourceLangId",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "#0000008A",
    Cell: (orgin) => {
      return (
        <div style={{ display: "flex" }}>
          <Typography
            component="h6"
            style={{ fontSize: 15, color: "#000000DE", fontFamily: "Rubik" }}
          >
            {language[orgin.original.sourceLangId - 1].name}
          </Typography>
        </div>
      );
    },
  },
  {
    Header: "Target Language Text",
    accessor: "sourceText",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "#000000B8",
  },
  {
    Header: "Audio",
    accessor: "sourceAudioName",
    width: 250,
    minWidth: 200,
    maxWidth: 300,
    color: "#0000008A",
    Cell: (orgin) => {
      return (
        <div style={{ display: "flex" }}>
          <AudiotrackIcon />
          <Typography
            component="h6"
            style={{ fontSize: 15, color: "#000000DE", fontFamily: "Rubik" }}
          >
            {orgin.original.sourceAudioName}
          </Typography>
        </div>
      );
    },
  },
  {
    Cell: (orgin) => {
      return (
        <div>
          <IconComp deleteIcon={true} id={orgin.index} getEditId={getEditId} />
        </div>
      );
    },
  },
];

const options = ["Edit Translation", "Delete Translation"];

export const TranslationColomn = (props) => {
  return [
    {
      Header: "TranslateId",
      accessor: "translateId",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "#0000008A",
    },
    {
      Header: "Source language",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "#000000B8",
      Cell: (orgin) => {
        return (
          <div style={{ display: "flex" }}>
            <Typography
              component="h6"
              style={{ fontSize: 15, color: "#000000DE", fontFamily: "Rubik" }}
            >
              Tamil
            </Typography>
          </div>
        );
      },
    },
    {
      Header: "Translate language",
      accessor: "groupName",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "#000000B8",
      Cell: (orgin) => {
        return (
          <div style={{ display: "flex" }}>
            <Typography
              component="h6"
              style={{ fontSize: 15, color: "#000000DE", fontFamily: "Rubik" }}
            >
              English
            </Typography>
          </div>
        );
      },
    },
    {
      Header: "English lang text",
      accessor: "englishText",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "green",
    },
    {
      Header: "Group id",
      accessor: "groupDetails",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "green",
      Cell: (orgin) => {
        console.log(orgin);
        return (
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <Typography
              component="h6"
              style={{ fontSize: 15, color: "#000000DE", fontFamily: "Rubik" }}
            >
              {orgin.original?.groupDetails?.name !== null
                ? orgin.original?.groupDetails?.name
                : ""}
            </Typography>
          </div>
        );
      },
    },
    {
      Header: "Sub Group",
      accessor: "subGroupDetails",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "#000000B8",
      Cell: (orgin) => {
        return (
          <div
            style={{
              width: "80%",
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <Typography
              component="h6"
              style={{ fontSize: 15, color: "#000000DE", fontFamily: "Rubik" }}
            >
              {orgin.original?.subGroupDetails?.name !== null
                ? orgin.original?.subGroupDetails?.name
                : ""}
            </Typography>
            <ProfilePopUp
              options={options}
              fun={props}
              id={orgin.original.id}
            />
          </div>
        );
      },
    },
  ];
};
