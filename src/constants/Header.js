import {EmsPath} from "../router/EmsPath"

export const menuTitle = [
	{
		titleName: "Masters",
		menuItem: [
			{
				subMenuItemName: "Conversation",
				subMenuItemPath: EmsPath.MASTERSCONVERSATIONList,
			},
			{
				subMenuItemName: "Translation",
				subMenuItemPath: EmsPath.MASTERSTRANSLATELIST,
			},
			{
				subMenuItemName: "Reading",
				subMenuItemPath: EmsPath.MASTERSREADINGList,
			}
		],
		key: "master",
	},
	{
		titleName: "AdCampaign",
		menuItem: [
			{
				subMenuItemName: "Company",
				subMenuItemPath: EmsPath.ADCAMPAIGNCOMPANYLIST,
			},
			{
				subMenuItemName: "Campaign Plan",
				subMenuItemPath: EmsPath.ADCAMPAIGNCAMPAIGNPLANLIST,
			}
		],
		key: "adcampaign",
	}
]

export const profileItem = [
	{
		profiletext: "Roles & Access",
	},
	{
		profiletext: "Our Employeees",
	},
	{
		profiletext: "Jockey Catagories",
	},
	{
		profiletext: "Prameters",
	},
	{
		profiletext: "Themes",
	},
	{
		profiletext: "Logout",
		active: true,
	},
]