import { Avatar, Button, IconButton, Typography } from "@material-ui/core";
import AudiotrackIcon from "@material-ui/icons/Audiotrack";
import EditIcon from '@material-ui/icons/Edit';
// import IconComp from "../component/reusables/IconComp"
import ProfilePopUp from "../../component/reusables/ProfilePopUp"

const options = ["Edit Plan","View Plan","Delete Plan"];
export const CampaignPlanTableSchema = (props) =>

{
  console.log(props)
  return(
    [
      {
        Header: "Company Name",
        accessor: "company.name",
        width: 250,
        minWidth: 200,
        maxWidth: 300,
        color: "#0000008A",
      },
      {
        Header: "Campaign duration",
        accessor: "startDate",
        width: 250,
        minWidth: 200,
        maxWidth: 300,
        color: "#000000B8",
      },
      {
        Header: "Campaign Type",
        accessor: "customCampaignTypeId",
        width: 250,
        minWidth: 200,
        maxWidth: 300,
        color: "green",
      },
      {
        Header: "Campaign Name",
        accessor: "name",
        width: 250,
        minWidth: 200,
        maxWidth: 300,
        color: "#000000B8",
      },
      {
        Header: "Contact Email",
        accessor: "company.userEmail",
        width: 250,
        minWidth: 200,
        maxWidth: 300,
        color: "#000000B8",
      },
      {
       
        width: 250,
        minWidth: 200,
        maxWidth: 300,
        color: "#000000B8",
        Cell: (orgin) => {
          console.log(orgin)
          return(
           
                <div style={{display:"flex",justifyContent:"center"}}>
                <ProfilePopUp options={options} fun={props} id={orgin.original.id}/>
                </div>
          )
        }
      },
    ]
  )
}