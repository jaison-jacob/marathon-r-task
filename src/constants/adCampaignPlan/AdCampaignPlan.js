export const adCampaignPlanState = {
	"name": "",
	"companyId": null,
	"startDate": null,
	"endDate": null,
	"startTime": null,
	"endTime": null,
	"languageId": 1,
	"countryId": 1,
	"countryName":"",
	"stateName": null,
	"districtName": null,
	"genderId": 1,
	"customAdDisplay": 1,
	"customCampaignType": 1,
	"fileName": "",
	"fileAwsName": "aws",
	"fileUrl": "aws",
	"allUsers": true,
	"users": [],
	"cityText":""
}

export const FileUploadKeys = {
	Path:"fileUrl",
	Name:"fileName",
	awsName:"fileAwsName",
  }