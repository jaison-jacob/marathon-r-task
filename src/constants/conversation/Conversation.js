export const conversationState = {
	"name" : "",
	"audioName": "",
	"audioAwsName": "",
	"audioType": "",
	"audioUrl": "",
  "groupId": null,
	"subGroupId": null,
	"person": [
	],
    "personData":{
        "firstPersonText": "",
        "firstPersonAudioPath": "",
        "firstPersonAudioName": "",
        "firstPersonAwsAudioName": "",
        "firstPersonAudioType": "",
        "secPersonText": "",
        "secPersonAudioPath": "",
        "secPersonAudioName": "",
        "secPersonAwsAudioName": "",
        "secPersonAudioType": ""
    },
    subFormActive:false,
}

export const mainFormAudioFileUploadKeys = {
  Path:"audioUrl",
  Name:"audioName",
  awsName:"audioAwsName",
  Type:"audioType",
}


export const conversationSubFormSchema = {
    feild1: {
      name: "personData.firstPersonText",
      label: "person 1 text",
    },
    feild2: {
      name: "personData.firstPersonAudioName",
      BtnName:"Audio",
      uploadFileKeys:{
        Path:"personData.firstPersonAudioPath",
        Name:"personData.firstPersonAudioName",
        awsName:"personData.firstPersonAwsAudioName",
        Type:"personData.firstPersonAudioType",
      }
    },
    feild3: {
      name: "personData.secPersonText",
      label: "person 2 text",
    },
    feild4: {
      name: "personData.secPersonAudioName",
      BtnName:"Audio",
      uploadFileKeys:{
        Path:"personData.secPersonAudioPath",
        Name:"personData.secPersonAudioName",
        awsName:"personData.secPersonAwsAudioName",
        Type:"personData.secPersonAudioType"
      }
    },
  };