import { Avatar, Button, IconButton, Typography } from "@material-ui/core";
import AudiotrackIcon from "@material-ui/icons/Audiotrack";
import EditIcon from '@material-ui/icons/Edit';
import {} from "../../"
import IconComp from "../../component/reusables/IconComp"
import ProfilePopUp from "../../component/reusables/ProfilePopUp"

export const conversationSubListColomn = (props) => [
    {
      Header: "Id",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "#0000008A",
      Cell: (orgin) => {
        console.log(orgin)
        return(
         
              <div style={{display:"flex"}}>
              <Typography
                component="h6"
                style={{ fontSize: 15,color:"#000000DE",fontFamily:"Rubik"}}
              >
                {orgin.index+1}
              </Typography>
              
              </div>
        )
      }
    },
    {
      Header: "person1",
      accessor: "firstPersonText",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "#000000B8",
    },
    {
      Header: "First Person Audio",
      accessor: "firstPersonAudioName",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "#0000008A",
      Cell: (orgin) => {
        console.log(orgin)
        return(
         
              <div style={{display:"flex"}}>
                <AudiotrackIcon/>
              <Typography
                component="h6"
                style={{ fontSize: 15,color:"#000000DE",fontFamily:"Rubik"}}
              >
                {orgin.original.firstPersonAudioName}
              </Typography>
              
              </div>
        )
      }
    },
    {
      Header: "person2",
      accessor: "secPersonText",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "green",
    },
    {
      Header: "Second Person Audio",
      accessor: "secPersonAudioName",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "#000000B8",
      Cell: (orgin) => {
        console.log(orgin)
        return(
         
              <div style={{display:"flex"}}>
                <AudiotrackIcon/>
              <Typography
                component="h6"
                style={{ fontSize: 15,color:"#000000DE",fontFamily:"Rubik"}}
              >
                {orgin.original.secPersonAudioName}
              </Typography>
              
              </div>
        )
      }
    },
    {
      
      Cell: (orgin) => {
        console.log(props,orgin)
        return(
         <div style={{marginLeft:"2rem"}}>
         <IconComp id={orgin.index} getEditId={props}/>  
              </div>
        )
      }
    },
  ];

  const options = ["Edit conversation","Delete Conversation"];

  export const conversationColomn = (props) =>

  {
    console.log(props)
    return(
      [
        {
          Header: "ConvoId",
          accessor: "conversationId",
          width: 250,
          minWidth: 200,
          maxWidth: 300,
          color: "#0000008A",
        },
        {
          Header: "Conversation name",
          accessor: "name",
          width: 250,
          minWidth: 200,
          maxWidth: 300,
          color: "green",
        },
        {
          Header: "Conversation Text",
          accessor: "groupName",
          width: 250,
          minWidth: 200,
          maxWidth: 300,
          color: "#000000B8",
          Cell: (orgin) => {
            console.log(props)
            return(
             
                  <div style={{display:"flex"}}>
                   
                  <Typography
                    component="h6"
                    style={{ fontSize: 15,color:"#000000DE",fontFamily:"Rubik"}}
                  >
                    Available
                  </Typography>
                 
                  </div>
            )
          }
        },
        {
          Header: "Group id",
          accessor: "groupDetails",
          width: 250,
          minWidth: 200,
          maxWidth: 300,
          color: "green",
          Cell: (orgin) => {
            console.log(orgin)
            return(
             
                  <div style={{display:"flex",justifyContent:"space-between"}}>
                   
                  <Typography
                    component="h6"
                    style={{ fontSize: 15,color:"#000000DE",fontFamily:"Rubik"}}
                  >
                    {orgin.original?.groupDetails?.name !== null ? orgin.original?.groupDetails?.name:""}
                  </Typography>
                 
                  </div>
            )
          }
        },
      {
        Header: "Sub Group",
        accessor: "subGroupDetails",
        width: 250,
        minWidth: 200,
        maxWidth: 300,
        color: "#000000B8",
        Cell: (orgin) => {
          return(
           
                <div style={{width:"80%",display:"flex",justifyContent:"space-between"}}>
                 
                <Typography
                  component="h6"
                  style={{ fontSize: 15,color:"#000000DE",fontFamily:"Rubik"}}
                >
                  {orgin.original?.subGroupDetails?.name !== null ? orgin.original?.subGroupDetails?.name:""}
                </Typography>
                <ProfilePopUp options={options} fun={props} id={orgin.original.id}/>
                </div>
          )
        }
      },
      ]
    )
  }