import { Avatar, Button, IconButton, Typography } from "@material-ui/core";
import AudiotrackIcon from "@material-ui/icons/Audiotrack";
import EditIcon from '@material-ui/icons/Edit';
import IconComp from "../component/reusables/IconComp"
import ProfilePopUp from "../component/reusables/ProfilePopUp"

export const conversationState = {
    conversationName:"",
    groupName:[],
    selectSubGroup:[],
    conversation:[],
    conversationSubState:{
        person1:"",
        person1Audio:"",
        person2:"",
        person2Audio:""
    }
}

export const conversationSubFormSchema = {
  feild1: {
    name: "conversationSubState.person1",
    label: "person 1 text",
  },
  feild2: {
    name: "conversationSubState.person1Audio",
  },
  feild3: {
    name: "conversationSubState.person2",
    label: "person 2 text",
  },
  feild4: {
    name: "conversationSubState.person2Audio",
  },
};

export const SubListColomn = (props) => [
    {
      Header: "Id",
      accessor: "id",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "#0000008A",
    },
    {
      Header: "person1 Text",
      accessor: "person1",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "#000000B8",
    },
    {
      Header: "Audio 1",
      accessor: "person1Audio",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "#0000008A",
      Cell: (orgin) => {
        console.log(orgin)
        return(
         
              <div style={{display:"flex"}}>
                <AudiotrackIcon/>
              <Typography
                component="h6"
                style={{ fontSize: 15,color:"#000000DE",fontFamily:"Rubik"}}
              >
                {orgin.original.person1Audio}
              </Typography>
              
              </div>
        )
      }
    },
    {
      Header: "person2 Text",
      accessor: "person2",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "green",
    },
    {
      Header: "Audio 2",
      accessor: "person2Audio",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "#000000B8",
      Cell: (orgin) => {
        console.log(orgin)
        return(
         
              <div style={{display:"flex"}}>
                <AudiotrackIcon/>
              <Typography
                component="h6"
                style={{ fontSize: 15,color:"#000000DE",fontFamily:"Rubik"}}
              >
                {orgin.original.person2Audio}
              </Typography>
              
              </div>
        )
      }
    },
    {
      
      Cell: (orgin) => {
        console.log(props,orgin)
        return(
         <div>
         <IconComp id={orgin.index} editSubListData={props.editSubListData}/>  
              </div>
        )
      }
    },
  ];

  export const MainListColomn = (props) =>

  {
    console.log(props)
    return(
      [
        {
          Header: "Id",
          accessor: "id",
          width: 250,
          minWidth: 200,
          maxWidth: 300,
          color: "#0000008A",
        },
        {
          Header: "Conversation Text",
          accessor: "text",
          width: 250,
          minWidth: 200,
          maxWidth: 300,
          color: "green",
        },
        {
          Header: "Group Name",
          accessor: "groupName",
          width: 250,
          minWidth: 200,
          maxWidth: 300,
          color: "#000000B8",
        },
        {
          Header: "sub Group",
          accessor: "selectSubGroup",
          width: 250,
          minWidth: 200,
          maxWidth: 300,
          color: "#000000B8",
          Cell: (orgin) => {
            console.log(props)
            return(
             
                  <div style={{display:"flex"}}>
                   
                  <Typography
                    component="h6"
                    style={{ fontSize: 15,color:"#000000DE",fontFamily:"Rubik"}}
                  >
                    {orgin.original.selectSubGroup}
                  </Typography>
                  <ProfilePopUp  fun={props} id={orgin.index}/>
                  </div>
            )
          }
        },
      ]
    )
  }
  

export const submitShapeChange = (values) => {
let copyValue = {...values}
  for(let item in values){
    if(item === "groupName" || item === "selectSubGroup"){
      // console.log(values[item])
      let res = values[item].map((e) => e.id)
      copyValue = {...copyValue,[item]:res}
    }
    if(item === "conversationSubState"){
      copyValue = {...copyValue,[item]:conversationState[item]}
    }
  }
return copyValue
}

export const changeEditToInitial = (values,groupName,selectSubGroup) =>{
  let copyValue = {...values}
  for(let item in values){
    if(item === "groupName" || item === "selectSubGroup"){
      console.log([item])
      // let res = values[item].map((e) => [item][])
      // copyValue = {...copyValue,[item]:res}
    }
    if(item === "conversationSubState"){
      copyValue = {...copyValue,[item]:conversationState[item]}
    }
  }
}