import { Avatar, Button, IconButton, Typography } from "@material-ui/core";
import AudiotrackIcon from "@material-ui/icons/Audiotrack";
import EditIcon from '@material-ui/icons/Edit';
import {} from "../../"
import IconComp from "../../component/reusables/IconComp"
import ProfilePopUp from "../../component/reusables/ProfilePopUp"

export const readingSubListColomn = (getEditId) => [
    {
      Header: "Question",
      accessor:"question",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "#0000008A",
    },
    {
      Header: "Audio",
      accessor: "questionAudioName",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "#0000008A",
      Cell: (orgin) => {
        console.log(orgin)
        return(
         
              <div style={{display:"flex"}}>
                <AudiotrackIcon/>
              <Typography
                component="h6"
                style={{ fontSize: 15,color:"#000000DE",fontFamily:"Rubik"}}
              >
                {orgin.original.questionAudioName}
              </Typography>
              
              </div>
        )
      }
    },
    {
      Header: "Answer",
      accessor: "answerText",
      width: 250,
      minWidth: 200,
      maxWidth: 300,
      color: "green",
    },
    {
      
      Cell: (orgin) => {
        console.log(getEditId,orgin)
        return(
         <div>
        <IconComp deleteIcon={true} id={orgin.index} getEditId={getEditId}/> 
              </div>
        )
      }
    },
  ];

  const options = ["Edit reading","Delete reading"];

  export const readingColomn = (props) =>

  {
    console.log(props)
    return(
      [
        {
          Header: "Story Id",
          accessor: "baseStoryNo",
          width: 250,
          minWidth: 200,
          maxWidth: 300,
          color: "#0000008A",
        },
        {
          Header: "Story name",
          accessor: "baseStoryName",
          width: 250,
          minWidth: 200,
          maxWidth: 300,
          color: "green",
        },
        {
          Header: "Question&Answer",
          accessor: "questionires",
          width: 250,
          minWidth: 200,
          maxWidth: 300,
          color: "#000000B8",
          Cell: (orgin) => {
            console.log(orgin)
            return(
             
                  <div style={{display:"flex"}}>
                   
                  <Typography
                    component="h6"
                    style={{ fontSize: 15,color:"#000000DE",fontFamily:"Rubik"}}
                  >
                    {orgin.original.questionires.length > 0 ? "Avilable" : "Not Available"}
                  </Typography>
                 
                  </div>
            )
          }
        },
        {
            Header: "Group",
            accessor: "groupDetails",
            width: 250,
            minWidth: 200,
            maxWidth: 300,
            color: "green",
            Cell: (orgin) => {
                console.log(props)
                return(
                 
                      <div style={{display:"flex"}}>
                       
                      <Typography
                        component="h6"
                        style={{ fontSize: 15,color:"#000000DE",fontFamily:"Rubik"}}
                      >
                        {orgin.original.groupDetails.name}
                      </Typography>
                     
                      </div>
                )
              }
          },
        {
          Header: "Sub Group",
          accessor: "subGroupDetails",
          width: 250,
          minWidth: 200,
          maxWidth: 300,
          color: "#000000B8",
          Cell: (orgin) => {
            console.log(props)
            return(
             
                  <div style={{display:"flex",justifyContent:"space-evenly"}}>
                   
                  <Typography
                    component="h6"
                    style={{ fontSize: 15,color:"#000000DE",fontFamily:"Rubik"}}
                  >
                    {orgin.original.subGroupDetails.name}
                  </Typography>
                  <ProfilePopUp options={options} fun={props} id={orgin.original.id}/>
                  </div>
            )
          }
        },
      ]
    )
  }