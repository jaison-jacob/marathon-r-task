
export const readingState = {
    "baseStoryName":"", 
      "baseStoryLang":null,
     "baseStoryImage":"",
     "imagePosition":1,
     "baseStoryText":"",
     "groupId":null,
     "subGroupId":null,
     "questionires":[],
  questioniresData:{
    "question": "",
        "questionAudio": "",
        "questionAudioName": "",
        "questionAwsAudioName": "",
        "questionAudioType": "",
    "answerText": "",
        "answerAudio": "",
        "answerAudioName": "",
        "answerAwsAudioName": "",
        "answerAudioType": ""
  },
     "keywords": [],
     subFormActive:false,
  }

  export const mainFormImageFileUploadKeys = {
    Name:"baseStoryImage"
  }

  

  export const conversationSubFormSchema = {
    feild1: {
      name: "questioniresData.question",
      label: "Question",
    },
    feild2: {
      name: "questioniresData.questionAudioName",
      BtnName:"Audio",
      uploadFileKeys:{
        Path:"questioniresData.questionAudio",
        Name:"questioniresData.questionAudioName",
        awsName:"questioniresData.questionAwsAudioName",
        Type:"questioniresData.questionAudioType",
      }
    },
    feild3: {
      name: "questioniresData.answerText",
      label: "Answer",
    },
    feild4: {
      name: "questioniresData.answerAudioName",
      BtnName:"Audio",
      uploadFileKeys:{
        Path:"questioniresData.answerAudio",
        Name:"questioniresData.answerAudioName",
        awsName:"questioniresData.answerAwsAudioName",
        Type:"questioniresData.answerAudioType"
      }
    },
  };