
import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  formContainer: {
    width: "80%",
    margin: `${theme.spacing(2)}px auto`,
  },
  product: {
    height: "64vh",
    overflowY: "auto",
    "&::-webkit-scrollbar": {
      display: "none",
    },
  },
  formHead: {
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(4),
  },
}));