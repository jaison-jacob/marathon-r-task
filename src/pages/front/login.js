import { Button } from '@material-ui/core'
import React from 'react'
import { Link, useHistory } from 'react-router-dom'
import {EmsPath} from "../../router/EmsPath"

function Login() {
    const history = useHistory();
    console.log("login loaded")
    return (
        <div>
            <h1>login page</h1>
            {/* <Button type="button" variant="outlined" onClick={()=>{history.push(EmsPath.MASTERSCONVERSATIONList)}}>nav</Button> */}
            <Link to={EmsPath.MASTERSCONVERSATIONList}>nav</Link>
        </div> 
    )
}

export default Login
