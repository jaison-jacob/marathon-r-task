import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
// import ErrorBoundary from "./ErrorBoundaries";
import Layout from "./Layout";
// import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";
import { MuiThemeProvider } from "@material-ui/core";
import { routes } from "../router/Routes";
import { formTheme } from "../theme/Formtheme";
import { connect } from "react-redux";
import { createSelector } from "reselect";
import { bindDispatch } from "../utils/BindDispatch";
import { ContectProvider } from "../context/Context";
import {CircularStatic} from "../component/reusables";
import { Header } from "../component/reusables/Header";
// import "../pages/styles/CommonTable.scss"


const Root = (props) => {
  console.log(props);
const {app} = props
  return (
    <MuiThemeProvider theme={formTheme}>
      <Router>
        <Switch>
          <div style={{ height: "calc(100vh - 64px)"}}>
          <Header/>
          <CircularStatic isloading={app.isLoading}/>
            <Switch>
              {routes.map((item, index) => (
                <Route
                  key={index}
                  exact
                  path={item.path}
                  render={(routeProps) => {
                    const { app, actions } = props;
                    return (
                      <ContectProvider>
                        <item.component
                          appState={app}
                          actions={actions}
                          {...routeProps}
                        />
                      </ContectProvider>
                    );
                  }}
                />
              ))}
            </Switch>
          </div>
        </Switch>
      </Router>
    </MuiThemeProvider>
  );
};

const mapStateToProps = createSelector(
  (state) => state.app,
  (app) => ({
    app,
  })
);

export default connect(mapStateToProps, bindDispatch)(Root);
