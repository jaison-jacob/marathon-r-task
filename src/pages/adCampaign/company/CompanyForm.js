import React, { useState, useEffect, useCallback, useRef } from "react";
import { Grid } from "@material-ui/core";
import {
  FormWrapper,
  FormHeader,
  FormFooter,
} from "../../../component/reusables";
import { Formik, Form, useFormikContext } from "formik";
import { companyState } from "../../../constants/company/Company";
import { companyFormValidationSchema } from "../../../validation/adCampaign/Company";
import { useStyles } from "../../styles/CommonFormContainer";
import { useHistory } from "react-router-dom";
import { EmsPath } from "../../../router/EmsPath";
import {
  getCompanyType,
  getCountry,
  getDistrictById,
  getState,
  createCompany,
  getCompanyById,
  updateCompany,
} from "../../../api/Api";
import CompanyForm from "../../../component/adCampaign/company";


function Company(props) {
  const styles = useStyles();
  const [companyType, setCompanyType] = useState([]);
  const [country, setCountry] = useState([]);
  const [state, setState] = useState([]);
  const [district, setDistrict] = useState([]);
  const formikRef = useRef();
  let history = useHistory();

  const setEditData = async (state) => {
    const {id,type} = state;
    let editCompanyData = await getCompanyById(id);
    let stateOption = await getStateData();
    let stateId =
      editCompanyData.data["countryId"] === 1
        ? getNameOfDistrict(
            stateOption,
            "name",
            editCompanyData.data["stateName"]
          ).id
        : null;
    let districtOption =
      editCompanyData.data["countryId"] === 1
        ? await getDistrictData(stateId)
        : [];

    let obj = {};

    Object.keys(editCompanyData.data).map((e) => {
      if (companyState.hasOwnProperty(e)) {
        obj[e] = editCompanyData.data[e];
      }
    });

    console.log(editCompanyData.data);
    console.log(districtOption, "name", editCompanyData.data["districtName"]);
    console.log(
      getNameOfDistrict(
        districtOption,
        "name",
        editCompanyData.data["districtName"]
      ).id
    );
    formikRef.current.setValues({
      ...obj,
      countryId: obj["countryId"],
      companyTypeId: obj["companyTypeId"],
      cityText:
        editCompanyData.data["countryId"] === 2
          ? editCompanyData.data["stateName"]
          : "",
      stateName: stateId,
      districtName:
        stateId !== null
          ? getNameOfDistrict(
              districtOption,
              "name",
              editCompanyData.data["districtName"]
            ).id
          : null,
          disabled:type.toLowerCase().includes("edit") ? false : true
    });
  };

  //submit form
  const mainFormSubmit = (value, Formikoption) => {
    let finalData = {
      ...value,
      stateName:
        value["countryId"] === 1
          ? getNameOfDistrict(state, "id", value["stateName"]).name
          : value["cityText"],
      districtName:
        value["countryId"] === 1
          ? getNameOfDistrict(district, "id", value["districtName"]).name
          : "",
    };
    console.log(finalData);
    if (history.location?.state?.id) {
      updateCompany(finalData, history.location?.state?.id).then((response) => {
        console.log(response);
        history.push(EmsPath.ADCAMPAIGNCOMPANYLIST);
      });
    } else {
      createCompany(finalData).then((response) => {
        console.log(response);
        history.push(EmsPath.ADCAMPAIGNCOMPANYLIST);
      });
    }
  };
  /* load all seeds */

  //get name of district
  const getNameOfDistrict = (data, type, val) => {
    return (
      data?.find?.(
        (op) => String(op[type]).toLowerCase() === String(val).toLowerCase()
      ) || {}
    );
  };

  //get state data
  const getStateData = useCallback(async () => {
    const res = await getState();
    setState(res.data);
    return res.data;
  }, []);

  //get company type data
  const getCompanyTypeData = useCallback(async () => {
    let response = await getCompanyType();
    setCompanyType(response.data);
    return response.data;
  }, []);

  //get district data
  const getDistrictData = useCallback(async (id) => {
    const res = await getDistrictById(id);
    setDistrict(res.data);
    return res.data;
  }, []);

  //get Country data
  const getCountryData = useCallback(async () => {
    let response = await getCountry();
    setCountry(response.data);
    return response.data;
  }, []);

  const setSeedData = async () => {
    await getCompanyTypeData();
    await getCountryData();
    await getStateData();
  };

  useEffect(() => {
    if (history.location?.state?.id) {
      console.log(history.location?.state?.id);
      setEditData(history.location?.state);
    }
    setSeedData();
  }, []);

  //dropdown onchange
  const dropDownOnchange = (eve, name, value, setFieldValue) => {
    console.log(eve, name, value, setFieldValue);
    if (name === "stateName" && value["countryId"] === 1) {
      getDistrictData(eve.target.value);
    }
    setFieldValue(name, eve.target.value);
  };

  //radio onchange 
  const radioOnchange = (eve, name, value, setFieldValue) =>{

  }

  //click cancel of Form this function is trigger
  const FormCancel = (type) => {
   
      history.push(EmsPath.ADCAMPAIGNCOMPANYLIST,{})
  
  };



  return (
    <div>
      <div>
        <div>
          <Grid container>
            <Grid item xs={1}></Grid>
            <Grid item xs={8} className={styles.formHead}>
              <FormHeader path={EmsPath.ADCAMPAIGNCOMPANYLIST} name="Company" />
            </Grid>
          </Grid>
          <Grid container className={styles.product}>
            <Grid item xs={1}></Grid>

            <Formik
              innerRef={formikRef}
              initialValues={{ ...companyState }}
              onSubmit={(value, r) => {
                console.log(r);
                mainFormSubmit(value, r);
              }}
              validationSchema={companyFormValidationSchema}
            >
              <>
                <Grid item xs={10}>
                  <Form onChange={(e) => console.log(e.target.value)}>
                    <CompanyForm
                      country={country}
                      companyType={companyType}
                      state={state}
                      district={district}
                      getDistrictById={getDistrictById}
                      setDistrict={setDistrict}
                      dropDownOnchange={dropDownOnchange}
                      
                    />
                  </Form>
                </Grid>
                <Grid item xs={12}>
                  <FormFooter updateText={history.location?.state?.id} FormCancel={FormCancel}/>
                </Grid>
              </>
            </Formik>
          </Grid>
        </div>
      </div>
    </div>
  );
}

export default Company;