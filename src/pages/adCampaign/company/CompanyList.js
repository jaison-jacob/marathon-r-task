import React, { useState, useEffect, useCallback } from "react";
import { Link, useHistory } from "react-router-dom";
import Nodata from "../../../component/reusables/Nodata";
import { EmsPath } from "../../../router/EmsPath";
import { CompanyTableSchema } from "../../../constants/company/CompanyTableSchema";
import { getCompany } from "../../../api/Api";
import { Urls } from "../../../api/Urls";
import { Grid } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import { CommonTable } from "../../../component/reusables";


import "react_custom_table/src/styles/index.scss";


function CompanyList(props) {
  let history = useHistory();


  const [companyTable, setcompanyTable] = useState({
    list: [],
    pagination: {
      pageSize: 10,
      currentPage: 1,
    },
    count: 0,
    search: "",
    filterData: {},
    currentSortIndex: null,
    isSorted: false,
    showPopup: false,
  });

  /**pagination handle  */
  const getList = useCallback(async (pageNumber, size) => {
    console.log(pageNumber, size);
    try {
      await getCompany(size, (pageNumber - 1) * size).then((res) => {
        const { data } = res;
        console.log(data);
        if (data?.count > 0) {
          setcompanyTable((prevState) => ({
            ...prevState,
            count: data.count,
            list: [...data.rows],
            pagination: { currentPage: pageNumber, pageSize: size },
          }));
        }
      });
    } catch (error) {
      console.log(error);
    }
  }, []);

  //get subList data from redux
  useEffect(() => {
    getList(
      companyTable.pagination.currentPage,
      companyTable.pagination.pageSize
    );
  }, []);

  const getEditId = async (index,type) => {
    if(type.toLowerCase().includes("delete")){
      console.log("delete")
    }else{
      let state = { id: index,type:type };
      await history.push(EmsPath.ADCAMPAIGNCOMPANYFORM, state);
    }
    
  };

  //filter data based on search
  const searchFilter = (val) => {
    let copyState = { ...companyTable };
    let newList = copyState.list.filter((o) =>
      Object.keys(o).some((k) => String(o[k]).includes(val))
    );
    setcompanyTable({ ...copyState, list: [...newList] });
  };
  const onPageNumberChange = (page) => {
    getList(page, companyTable.pagination.pageSize);
  };
  const onChangePageSize = (newPageSize) => {
    getList(1, newPageSize);
  };
  

  return (
    <div  style={{ padding: "20px 30px" }}>
      <Grid container>
        {companyTable.list.length === 0 ? (
          <Grid item xs={12}>
            <Nodata
              text="Company"
              btnText="New Company"
              path={EmsPath.ADCAMPAIGNCOMPANYFORM}
            />
          </Grid>
        ) : (
          <Grid item xs={12}>
            <CommonTable
              columns={CompanyTableSchema(getEditId)}
              data={companyTable.list}
              selection={[]}
              LoadingComponent={() => <div></div>}
              tableControlProps={{
                tableTitle: "Company",
                search: true,
                filter: false,
                addNew: true,
                download: false,
                searchProps: {
                  placeHolder: "search...",
                  onSearchChange: (e) => searchFilter(e.target.value),
                },

                addButtonProps: {
                  label: "New Company",
                  leftIcon: <div></div>,
                  rightIcon: <div></div>,
                  onClick: () => history.push(EmsPath.ADCAMPAIGNCOMPANYFORM),
                },
              }}
              totalCount={companyTable.count}
            pageSize={companyTable.pagination.pageSize}
            currentPage={companyTable.pagination.currentPage}
            onPageNumberChange={onPageNumberChange}
            onChangePageSize={onChangePageSize}
            showPagination={true}
            
            />
          </Grid>
        )}
      </Grid>
    </div>
  );
}

export default CompanyList;
