import React, { useState, useEffect, useCallback } from "react";
import { Link, useHistory } from "react-router-dom";
import Nodata from "../../../component/reusables/Nodata";
import { EmsPath } from "../../../router/EmsPath";
import { CampaignPlanTableSchema } from "../../../constants/adCampaignPlan/AdCampaignPlanTableSchema";
import { getCampaignPlan } from "../../../api/Api";
import { Urls } from "../../../api/Urls";
import { Grid } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import { CommonTable } from "../../../component/reusables";
import "react_custom_table/src/styles/index.scss";

function CampaignPLanList(props) {
  let history = useHistory();
  // campaignPlanTable
  const [campaignPlanTable, setCampaignPlanTable] = useState({
    list: [],
    pagination: {
      pageSize: 10,
      currentPage: 1,
    },
    selection:[],
    count: 0,
    search: "",
    filterData: {},
    currentSortIndex: null,
    isSorted: false,
    showPopup: false,
  });

  /**pagination handle  */
  const getList = useCallback(async (pageNumber, size) => {
    console.log(pageNumber, size);
    try {
      await getCampaignPlan(size, (pageNumber - 1) * size).then((res) => {
        const { data } = res;
        console.log(data);
        if (data?.count > 0) {
          setCampaignPlanTable((prevState) => ({
            ...prevState,
            count: data.count,
            list: [...data.rows],
            pagination: { currentPage: pageNumber, pageSize: size },
          }));
        }
      });
    } catch (error) {
      console.log(error);
    }
  }, []);

  //get subList data from redux
  useEffect(() => {
    getList(
      campaignPlanTable.pagination.currentPage,
      campaignPlanTable.pagination.pageSize
    );
  }, []);

  const getEditId = async (index,type) => {
    let state = { id: index,type:type };
    await history.push(EmsPath.ADCAMPAIGNCAMPAIGNPLANFORM, state);
  };

  //filter data based on search
  const searchFilter = (val) => {
    let copyState = { ...campaignPlanTable };
    let newList = copyState.list.filter((o) =>
      Object.keys(o).some((k) => String(o[k]).includes(val))
    );
    setCampaignPlanTable({ ...copyState, list: [...newList] });
  };

  const onPageNumberChange = (page) => {
    getList(page, campaignPlanTable.pagination.pageSize);
  };
  const onChangePageSize = (newPageSize) => {
    getList(1, newPageSize);
  };

  return (
    <div  style={{ padding: "20px 30px" }}>
      <Grid container>
        {campaignPlanTable.list.length === 0 ? (
          <Grid item xs={12}>
            <Nodata
              text="Company Plan"
              btnText="New Plan"
              path={EmsPath.ADCAMPAIGNCAMPAIGNPLANFORM}
            />
          </Grid>
        ) : (
          <Grid item xs={12}>
            <CommonTable
              columns={CampaignPlanTableSchema(getEditId)}  
              data={campaignPlanTable.list}
              selectable={true}
              selection={campaignPlanTable.selection}
              onSelect={(selections) => setCampaignPlanTable((prevState) => ({
                ...prevState,
                selection:selections, 
              }))}
             
              selectedBackground="rgba(121, 126, 209, 0.2)"
              LoadingComponent={() => <div></div>}
              tableControlProps={{
                tableTitle: "Campaign",
                search: true,
                filter: false,
                addNew: true,
                download: false,
                searchProps: {
                  placeHolder: "search...",
                  onSearchChange: (e) => searchFilter(e.target.value),
                },
                addButtonProps: {
                  label: "New Plan",
                  leftIcon: <div></div>,
                  rightIcon: <div></div>,
                  onClick: () => history.push(EmsPath.ADCAMPAIGNCAMPAIGNPLANFORM),
                },
              }}
              totalCount={campaignPlanTable.count}
            pageSize={campaignPlanTable.pagination.pageSize}
            currentPage={campaignPlanTable.pagination.currentPage}
            onPageNumberChange={onPageNumberChange}
            onChangePageSize={onChangePageSize}
            showPagination={true}
              
            />
          </Grid>
        )}
      </Grid>
    </div>
  );
}

export default CampaignPLanList;
