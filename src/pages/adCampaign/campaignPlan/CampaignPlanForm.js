import React, { useState, useEffect, useCallback, useRef } from "react";
import { Grid } from "@material-ui/core";
import {
  FormHeader,
  FormFooter,
} from "../../../component/reusables";
import { Formik, Form } from "formik";
import { adCampaignPlanState,FileUploadKeys } from "../../../constants/adCampaignPlan/AdCampaignPlan";
import { adCampaignPlanFormValidationSchema } from "../../../validation/adCampaign/adCampaignPlan";
import { useStyles } from "../../styles/CommonFormContainer";
import { useHistory } from "react-router-dom";
import { EmsPath } from "../../../router/EmsPath";
import {
  getCountry,
  getDistrictById,
  getState,
  getLanguage,
  getGender,
  getFileType,
  getUsers,
  getAllCompany,
  createCampaignPLan,
  getCampaignPlanById,
  updateCampaignPLan
} from "../../../api/Api";
import PlanDetail from "../../../component/adCampaign/capaignPlan/PlanDetail";
import CostomizedAd from "../../../component/adCampaign/capaignPlan/CostomizedAd";


function CampaignPlanForm(props) {
  const styles = useStyles();
  const [company, setCompany] = useState([]);
  const [country, setCountry] = useState([]);
  const [state, setState] = useState([]);
  const [district, setDistrict] = useState([]);
  const [language, setLanguage] = useState([]);
  const [gender, setGender] = useState([]);
  const [users, setUsers] = useState([]);
  const [fileType, setFileType] = useState([]);
  const formikRef = useRef();
  let history = useHistory();

 

  const setEditData = async (state) => {
    const { id, type } = state;
    console.log(id)
    let editCampaignPLanData = await getCampaignPlanById(id);
    console.log(editCampaignPLanData)
    let stateOption = await getStateData();
    let stateId =
      editCampaignPLanData.data["countryId"] === 1
        ? getNameOfDistrict(
            stateOption,
            "name",
            editCampaignPLanData.data["stateName"]
          ).id
        : null;
    let districtOption =
      editCampaignPLanData.data["countryId"] === 1
        ? await getDistrictData(stateId)
        : [];

    let obj = {};

    Object.keys(editCampaignPLanData.data).map((e) => {
      if (adCampaignPlanState.hasOwnProperty(e)) {
        obj[e] = editCampaignPLanData.data[e];
      }
    });

    console.log(editCampaignPLanData.data);
    console.log(districtOption, "name", editCampaignPLanData.data["districtName"]);
    console.log(
      getNameOfDistrict(
        districtOption,
        "name",
        editCampaignPLanData.data["districtName"]
      ).id
    );
    formikRef.current.setValues({
      ...obj,
      countryId: obj["countryId"],
      companyTypeId: obj["companyTypeId"],
      cityText:
        editCampaignPLanData.data["countryId"] === 2
          ? editCampaignPLanData.data["stateName"]
          : "",
      stateName: stateId,
      districtName:
        stateId !== null
          ? getNameOfDistrict(
              districtOption,
              "name",
              editCampaignPLanData.data["districtName"]
            ).id
          : null,
          users: [editCampaignPLanData.data.campaignUsers[0].users],
      disabled: type.toLowerCase().includes("edit") ? false : true,
    });
  };

  //submit form
  const mainFormSubmit = (value, Formikoption) => {
    console.log(value);
    let finalData = {
      ...value,
      stateName:
        value["countryId"] === 1
          ? getNameOfDistrict(state, "id", value["stateName"]).name
          : value["cityText"],
      countryName: value["countryId"] === 1 ? "" : value["countryName"],
      districtName:
        value["countryId"] === 1
          ? getNameOfDistrict(district, "id", value["districtName"]).name
          : "",
      allUsers: users.length === value["users"].length ? true : false,
      customAdDisplay: value["customAdDisplay"] === 1 ? true : false,
      users: value["users"].map((item) => item.id),
    };
    console.log(finalData);
    if (history.location?.state?.id) {
      updateCampaignPLan(finalData, history.location?.state?.id).then((response) => {
        console.log(response);
        history.push(EmsPath.ADCAMPAIGNCAMPAIGNPLANLIST, {});
      }).catch((e) =>{
        console.log(e)
      })
    } else {
      createCampaignPLan(finalData).then((response) => {
        console.log(response);
        history.push(EmsPath.ADCAMPAIGNCAMPAIGNPLANLIST, {});
      });
    }
  };
  /* load all seeds */

  //get name of district
  const getNameOfDistrict = (data, type, val) => {
    return (
      data?.find?.(
        (op) => String(op[type]).toLowerCase() === String(val).toLowerCase()
      ) || {}
    );
  };

  //get state data
  const getStateData = useCallback(async () => {
    const res = await getState();
    setState(res.data);
    return res.data;
  }, []);

  //get company type data
  const getCompany = useCallback(async () => {
    let response = await getAllCompany();
    console.log(response);
    setCompany(response.data.rows);
    return response.data.rows;
  }, []);

  //get district data
  const getDistrictData = useCallback(async (id) => {
    const res = await getDistrictById(id);
    setDistrict(res.data);
    return res.data;
  }, []);

  //get Country data
  const getCountryData = useCallback(async () => {
    let response = await getCountry();
    setCountry(response.data);
    return response.data;
  }, []);

  ///get Language data
  const getLanguageData = useCallback(async () => {
    let response = await getLanguage();
    setLanguage(response.data);
    return response.data;
  }, []);

  ///get gender data
  const getGenderData = useCallback(async () => {
    let response = await getGender();
    setGender(response.data);
    return response.data;
  }, []);

  ///get filetype data
  const getFileTypeData = useCallback(async () => {
    let response = await getFileType();
    setFileType(response.data);
    return response.data;
  }, []);

  ///get Users data
  const getUsersData = useCallback(async () => {
    let response = await getUsers();
    setUsers(response.data.rows);
    return response.data.rows;
  }, []);

  const setSeedData = async () => {
    await getCompany();
    await getCountryData();
    await getStateData();
    await getUsersData();
    await getFileTypeData();
    await getGenderData();
    await getLanguageData();
  };

  useEffect(() => {
    if (history.location?.state?.id) {
      console.log(history.location?.state);
      setEditData(history.location?.state);
    }
    setSeedData();
  }, []);

  //dropdown onchange
  const dropDownOnchange = (eve, name, value, setFieldValue) => {
    console.log(eve, name, value, setFieldValue);
    if (name === "stateName" && value["countryId"] === 1) {
      getDistrictData(eve.target.value);
    }
    setFieldValue(name, eve.target.value);
  };

  //radio onchange
  const radioOnchange = (eve, name, value, setFieldValue) => {
    console.log(eve, name, value, setFieldValue);
    if (name === "stateName" && value["countryId"] === 1) {
      getDistrictData(eve.target.value);
    }
    setFieldValue(name, Number(eve.target.value));
  };
 
  

  

  //click cancel of Form this function is trigger
  const FormCancel = (type) => {
      history.push(EmsPath.ADCAMPAIGNCAMPAIGNPLANLIST,{})
  };

  return (
    <div>
      <div>
        <div>
          <Grid container>
            <Grid item xs={1}></Grid>
            <Grid item xs={8} className={styles.formHead}>
              <FormHeader path={EmsPath.ADCAMPAIGNCAMPAIGNPLANLIST} name="Campaign Plan" />
            </Grid>
          </Grid>
          <Grid container className={styles.product}>
            <Grid item xs={1}></Grid>
            <Formik
              innerRef={formikRef}
              initialValues={{ ...adCampaignPlanState }}
              onSubmit={(value, r) => {
                console.log(value);
                mainFormSubmit(value, r);
              }}
              validationSchema={adCampaignPlanFormValidationSchema}
            >
              <>
                <Grid item xs={10}>
                  <Form onChange={(e) => console.log(e.target.value)}>
                    <PlanDetail
                      country={country}
                      companyType={company}
                      state={state}
                      district={district}
                      getDistrictById={getDistrictById}
                      setDistrict={setDistrict}
                      dropDownOnchange={dropDownOnchange}
                      radioOnchange={radioOnchange}
                      language={language}
                      gender={gender}
                      users={users}
                      fileType={fileType}
                    />
                    <CostomizedAd
                      fileType={fileType}
                      radioOnchange={radioOnchange}
                      FileUploadKeys={FileUploadKeys}
                    />
                  </Form>
                </Grid>
                <Grid item xs={12}>
                  <FormFooter updateText={history.location?.state?.id} FormCancel={FormCancel}/>
                </Grid>
              </>
            </Formik>
          </Grid>
        </div>
      </div>
    </div>
  );
}
export default CampaignPlanForm;
