import { Grid } from "@material-ui/core";
import React, { useState, useEffect,useCallback } from "react";
import { Link, useHistory } from "react-router-dom";
import { EmsPath } from "../../../router/EmsPath";
import { CommonTable } from "../../../component/reusables";
import { TranslationColomn } from "../../../constants/translate/TranslateTableSchema";
import AddIcon from '@material-ui/icons/Add';
import Nodata from "../../../component/reusables/Nodata";
import {getTranslation} from "../../../api/Api"
import "react_custom_table/src/styles/index.scss";

function TranslationList(props) {
  let history = useHistory();
  // let Styles = useStyles();

  const [translationTable, settranslationTable] = useState({
    list: [],
    pagination: {
      pageSize: 10,
      currentPage: 1,
    },
    selection:[],
    count: 0,
    search: "",
    filterData: {},
    currentSortIndex: null,
    isSorted: false,
    showPopup: false,
  });

  /**pagination handle  */
  const getList = useCallback(async (pageNumber, size) => {
    console.log(pageNumber, size);
    try {
      await getTranslation(size, (pageNumber - 1) * size).then((res) => {
        const { data } = res;
        console.log(data);
        if (data?.count > 0) {
          settranslationTable((prevState) => ({
            ...prevState,
            count: data.count,
            list: [...data.rows],
            pagination: { currentPage: pageNumber, pageSize: size },
          }));
        }
      });
    } catch (error) {
      console.log(error);
    }
  }, []);

  //get subList data from redux
  useEffect(() => {
    getList(
      translationTable.pagination.currentPage,
      translationTable.pagination.pageSize
    );
  }, []);

  const getEditId = async (index,type) => {
    let state = { id: index,type:type };
    await history.push(EmsPath.MASTERSTRANSLATEFORM, state);
  };

  //filter data based on search
  const searchFilter = (val) => {
    let copyState = { ...translationTable };
    let newList = copyState.list.filter((o) =>
      Object.keys(o).some((k) => String(o[k]).includes(val))
    );
    settranslationTable({ ...copyState, list: [...newList] });
  };

  const onPageNumberChange = (page) => {
    getList(page, translationTable.pagination.pageSize);
  };
  const onChangePageSize = (newPageSize) => {
    getList(1, newPageSize);
  };

  return (
    <Grid container style={{padding:"20px 30px"}}>
      {translationTable.list.length === 0 ? (
        <Grid item xs={12}>
          <Nodata
          text="Translation"
          btnText="New"
          path={EmsPath.MASTERSTRANSLATEFORM}
          />
        </Grid>
      ) : (
        <Grid item xs={12}>
          <CommonTable
            columns={TranslationColomn(getEditId)}
            data={translationTable.list}
            selection={[]}
            LoadingComponent={() => <div></div>}
            tableControlProps={{
              tableTitle: "Translation",
              search: true,
              filter: false,
              addNew: true,
              download: false,
              addButtonProps: {
                label: "New",
                leftIcon: <div></div>,
                rightIcon: <div></div>,
                onClick: () => history.push(EmsPath.MASTERSTRANSLATEFORM),
              }
            }}
            totalCount={translationTable.count}
            pageSize={translationTable.pagination.pageSize}
            currentPage={translationTable.pagination.currentPage}
            onPageNumberChange={onPageNumberChange}
            onChangePageSize={onChangePageSize}
            showPagination={true}
          />
        </Grid>
      )}
    </Grid>
  );
}


export default TranslationList;