import React, { useState, useEffect, useRef, useCallback } from "react";
import { Grid} from "@material-ui/core";
import {
  FormHeader,
  FormFooter,
} from "../../../component/reusables";
import { Formik, Form} from "formik";

import {
  translateState,
  translateEnglishFileUploadKeys,
  translateTargetLanguageUploadKeys
} from "../../../constants/translate/Translate";
import { translateSubListColomn } from "../../../constants/translate/TranslateTableSchema";
import TranslateMainForm from "../../../component/masters/translate/TranslateMainForm";
import TranslateSubForm from "../../../component/masters/translate/TranslateSubForm"
import { translationValidationSchema } from "../../../validation/masters/Translate";
import { useStyles } from "../../styles/CommonFormContainer";
import { useHistory } from "react-router-dom";
import { EmsPath } from "../../../router/EmsPath";
import {
  getGroups,
  getSubGroupsById,
  getLanguage,
  createTranslation,
  updateTranslation,
  getTranslationById
} from "../../../api/Api";

function TranslationForm(props) {
  let history = useHistory();
  const styles = useStyles();
  const formikRef = useRef();
  const [groups, setGroups] = useState([]);
  const [subGroups, setSubGroups] = useState([]);
  const [language,setLanguage] = useState([])
  const [subFormEditIndex, setsubFormEditIndex] = useState(null);

  //set edit value to feilds
  const setEditData = async (state) => {
    const { id, type } = state;
  
    let editTranslationData = await getTranslationById(id);
    editTranslationData = {...editTranslationData.data};
   
    await getGroupsData();
    await getSubGroupData(editTranslationData["groupId"]);
    let obj = {};
    Object.keys(editTranslationData).map((e) => {
      if (translateState.hasOwnProperty(e)) {
        obj[e] = editTranslationData[e];
      }
    });
    formikRef.current.setValues({
      ...obj,
      translationDetails: editTranslationData["translateSourceLanguage"],
      translateData: translateState["translateData"],
    });
  };
 
  //click cancel of Form this function is trigger
  const FormCancel = (type) => {
    if(type === "subForm"){
      let copyTranslateData = { ...translateState["translateData"] };
      formikRef.current.setFieldValue("translateData", copyTranslateData);
    }else{
      history.push(EmsPath.MASTERSTRANSLATELIST,{})
    }
  
  };

  //form submition
  const formSubmit = (value, r) => {
    if (value["subFormActive"] === true) {
      let copyTranslation = [...value["translationDetails"]];
      let copyTranslateData = { ...translateState["translateData"] };
      formikRef.current.setTouched({});
      formikRef.current.setSubmitting(false);

      if (subFormEditIndex === null) {
        copyTranslation.push(value["translateData"]);
      } else {
        copyTranslation[subFormEditIndex] = { ...value["translateData"] };
        setsubFormEditIndex(null);
      }

      formikRef.current.setFieldValue("translationDetails", copyTranslation);
      formikRef.current.setFieldValue("translateData", copyTranslateData);
    } else {
     
      let finalData = {
        ...value
       
      };
console.log(finalData)
      if (history.location?.state?.id) {
        console.log("IN",finalData)
        updateTranslation(finalData, history.location?.state?.id).then(
          (res) => {
            console.log(res)
            history.push(EmsPath.MASTERSTRANSLATELIST, {});
          }
        ).catch((e) => {
          console.log(e)
        })
      } else {
        
        createTranslation(finalData).then((res) => {
          console.log(res)
          history.push(EmsPath.MASTERSTRANSLATELIST, {});
        });
      }
    }
  };

 

  ///get subGroup data
  const getSubGroupData = useCallback(async (id) => {
    let response = await getSubGroupsById(id);
    setSubGroups(response.data);
    return response.data;
  }, []);

  ///get Groups data
  const getGroupsData = useCallback(async () => {
    let response = await getGroups();
    setGroups(response.data);
    return response.data;
  }, []);

  ///get language data
  const getLanguageData = useCallback(async () => {
    let response = await getLanguage();
    setLanguage(response.data);
    return response.data;
  }, []);

  const setSeedData = async () => {
    await getGroupsData();
    await getLanguageData();
  };

  useEffect(() => {
    if (history.location?.state?.id) {
      setEditData(history.location?.state);
    }
    setSeedData();
  }, []);

  //dropdown onchange
  const dropDownOnchange = (eve, name, value, setFieldValue) => {
    if (name === "groupId") {
      getSubGroupData(eve.target.value);
    }
    setFieldValue(name, eve.target.value);
  };

  //get subform id
  const getEditId = (id,type) => {
    let editValue = [ ...formikRef.current.values["translationDetails"]];
  if(type === "edit"){
    formikRef.current.setFieldValue("translateData", editValue[id]);
    setsubFormEditIndex(id);
  }else{
    let c = editValue.filter((item,index) => index !== id)
    formikRef.current.setFieldValue("translationDetails", [...c]);
  } 
  };

  //set active form status
  const changeActiveForm = (type) =>{
    if(type === "main"){
      if(formikRef.current.values["subFormActive"] === true){
        formikRef.current.setErrors({})
        formikRef.current.setTouched({})
      }
      formikRef.current.setFieldValue("subFormActive", false)
    }else{
      if(formikRef.current.values["subFormActive"] === false){
        formikRef.current.setErrors({})
        formikRef.current.setTouched({})
      }
     
      formikRef.current.setFieldValue("subFormActive", true)
    }
  }

  return (
    <div>
      <div>
        <div>
          <Grid container>
            <Grid item xs={1}></Grid>
            <Grid item xs={8} className={styles.formHead}>
              <FormHeader path={EmsPath.MASTERSTRANSLATELIST} name="Translate" />
            </Grid>
          </Grid>
          <Grid container className={styles.product}>
            <Grid item xs={1}></Grid>

            <Formik
              initialValues={{ ...translateState }}
              validationSchema={translationValidationSchema}
              innerRef={formikRef}
              onSubmit={(value, r) => {
                formSubmit(value, r);
              }}
            >
              <>
                <Grid item xs={10}>
                  <Form
                    onClick={(e) => changeActiveForm("main")
                      
                    }
                  >
                    <TranslateMainForm
                      groups={groups}
                      subGroups={subGroups}
                      dropDownOnchange={dropDownOnchange}
                      mainFormAudioFileUploadKeys={translateEnglishFileUploadKeys}
                    />
                  </Form>
                  <Form
                    onClick={(e) =>
                      changeActiveForm("sub")
                    }
                  >
                    <TranslateSubForm
                      COLUMNS={translateSubListColomn}
                      FormCancel={FormCancel}
                      SubFormUploadKeys={translateTargetLanguageUploadKeys}
                      subFormEditIndex={subFormEditIndex}
                      getEditId={getEditId}
                      errorName="translationDetails"
                      subFormHeaderText="Translate"
                      formKey="subFormActive"
                      language={language}
                      dropDownOnchange={dropDownOnchange}
                    />
                  </Form>
                </Grid>
                <Grid item xs={12}>
                  <FormFooter updateText={history.location?.state?.id} formKey="subFormActive" FormCancel={FormCancel}/>
                </Grid>
              </>
            </Formik>
          </Grid>
        </div>
      </div>
    </div>
  );
}

export default TranslationForm;