import { Grid } from "@material-ui/core";
import React, { useState, useEffect, useCallback } from "react";
import { Link, useHistory } from "react-router-dom";
import { EmsPath } from "../../../router/EmsPath";
import { CommonTable } from "../../../component/reusables";
import { conversationColomn } from "../../../constants/conversation/ConversationTableSchema";
import AddIcon from "@material-ui/icons/Add";
import Nodata from "../../../component/reusables/Nodata";
import { getConversation } from "../../../api/Api";
import "react_custom_table/src/styles/index.scss";
// import "../../styles/CommonTable.scss"


function ConversationList(props) {
  console.log(props.actions.appLoading);
  let history = useHistory();
  // let Styles = useStyles();

  const [conversationTable, setCampaignPlanTable] = useState({
    list: [],
    pagination: {
      pageSize: 10,
      currentPage: 1,
    },
    selection: [],
    count: 0,
    search: "",
    filterData: {},
    currentSortIndex: null,
    isSorted: false,
    showPopup: false,
  });

  /**pagination handle  */
  const getList = useCallback(async (pageNumber, size) => {
    console.log(pageNumber, size);
    try {
      await getConversation(size, (pageNumber - 1) * size).then((res) => {
        const { data } = res;
        console.log(data);
        if (data?.count > 0) {
          setCampaignPlanTable((prevState) => ({
            ...prevState,
            count: data.count,
            list: [...data.rows],
            pagination: { currentPage: pageNumber, pageSize: size },
          }));
        }
      });
    } catch (error) {
      console.log(error);
    }
  }, []);

  //get subList data from redux
  useEffect(() => {
    getList(
      conversationTable.pagination.currentPage,
      conversationTable.pagination.pageSize
    );
  }, []);

  const getEditId = async (index, type) => {
    let state = { id: index, type: type };
    await history.push(EmsPath.MASTERSCONVERSATIONFORM, state);
  };

  //filter data based on search
  const searchFilter = (val) => {
    let copyState = { ...conversationTable };
    let newList = copyState.list.filter((o) =>
      Object.keys(o).some((k) => String(o[k]).includes(val))
    );
    setCampaignPlanTable({ ...copyState, list: [...newList] });
  };

  const onPageNumberChange = (page) => {
    getList(page, conversationTable.pagination.pageSize);
  };
  const onChangePageSize = (newPageSize) => {
    getList(1, newPageSize);
  };
// console.log(conversationTable.pagination.pageSize,conversationTable.cu)
  return (
    <Grid container style={{ padding: "20px 30px" }}>
      {conversationTable.list.length === 0 ? (
        <Grid item xs={12}>
          <Nodata
            text="conversation"
            btnText="create conversation"
            path={EmsPath.MASTERSCONVERSATIONFORM}
          />
        </Grid>
      ) : (
        <Grid item xs={12}>
          <CommonTable
            columns={conversationColomn(getEditId)}
            data={conversationTable.list}
            selection={[]}
            LoadingComponent={() => <div></div>}
            tableControlProps={{
              tableTitle: "conversation",
              search: true,
              filter: false,
              addNew: true,
              download: false,
              addButtonProps: {
                label: "New conversation",
                leftIcon: <div></div>,
                rightIcon: <div></div>,
                onClick: () => history.push(EmsPath.MASTERSCONVERSATIONFORM),
              },
            }}
            totalCount={conversationTable.count}
            pageSize={conversationTable.pagination.pageSize}
            currentPage={conversationTable.pagination.currentPage}
            onPageNumberChange={onPageNumberChange}
            onChangePageSize={onChangePageSize}
            showPagination={true}
          />
        </Grid>
      )}
    </Grid>
  );
}

export default ConversationList;
