import React, { useState, useEffect, useRef, useCallback } from "react";
import { Grid } from "@material-ui/core";
import { FormHeader, FormFooter } from "../../../component/reusables";
import { Formik, Form } from "formik";

import {
  conversationState,
  conversationSubFormSchema,
  mainFormAudioFileUploadKeys,
} from "../../../constants/conversation/Conversation";
import { conversationSubListColomn } from "../../../constants/conversation/ConversationTableSchema";
import ConversationMainForm from "../../../component/masters/conversation";
import { conversationValidationSchema } from "../../../validation/masters/conversation";
import { useStyles } from "../../styles/CommonFormContainer";
import { useHistory } from "react-router-dom";
import { EmsPath } from "../../../router/EmsPath";
import CommonSubForm from "../../../component/masters/CommonSubForm";
import {
  getGroups,
  getSubGroupsById,
  createConversation,
  getConversationById,
  updateConversation,
} from "../../../api/Api";

function ConversationForm(props) {
  let history = useHistory();
  const styles = useStyles();
  const formikRef = useRef();
  const [groups, setGroups] = useState([]);
  const [subGroups, setSubGroups] = useState([]);
  const [subFormEditIndex, setsubFormEditIndex] = useState(null);

  //set edit value to feilds
  const setEditData = async (state) => {
    const { id, type } = state;
    console.log(id);
    let editCampaignPLanData = await getConversationById(id);
    editCampaignPLanData = editCampaignPLanData.data;
    await getGroupsData();
    await getSubGroupData(editCampaignPLanData["groupId"]);
    let obj = {};
    Object.keys(editCampaignPLanData).map((e) => {
      if (conversationState.hasOwnProperty(e)) {
        obj[e] = editCampaignPLanData[e];
      }
    });
    formikRef.current.setValues({
      ...obj,
      person: editCampaignPLanData["conversationDetails"],
      personData: conversationState["personData"],
    });
  };

  //click cancel of Form this function is trigger
  const FormCancel = (type) => {
    if (type === "subForm") {
      let copyPersonData = { ...conversationState["personData"] };
      formikRef.current.setFieldValue("personData", copyPersonData);
    } else {
      history.push(EmsPath.MASTERSCONVERSATIONList, {});
    }
  };

  //form submition
  const formSubmit = (value, r) => {
    if (value["subFormActive"] === true) {
      let copyPerson = [...value["person"]];
      let copyPersonData = { ...conversationState["personData"] };
      formikRef.current.setTouched({});
      formikRef.current.setSubmitting(false);

      if (subFormEditIndex === null) {
        copyPerson.push(value["personData"]);
      } else {
        copyPerson[subFormEditIndex] = { ...value["personData"] };
        setsubFormEditIndex(null);
      }

      formikRef.current.setFieldValue("person", copyPerson);
      formikRef.current.setFieldValue("personData", copyPersonData);
    } else {
      console.log(value);
      let finalData = {
        ...value,
        groupId: [value["groupId"]],
        subGroupId: [value["subGroupId"]],
      };

      if (history.location?.state?.id) {
        updateConversation(finalData, history.location?.state?.id).then(
          (res) => {
            console.log(res);
            history.push(EmsPath.MASTERSCONVERSATIONList, {});
          }
        );
      } else {
        createConversation(finalData).then((res) => {
          console.log(res);
          history.push(EmsPath.MASTERSCONVERSATIONList, {});
        });
      }
    }
  };

  ///get filetype data
  const getSubGroupData = useCallback(async (id) => {
    let response = await getSubGroupsById(id);
    setSubGroups(response.data);
    return response.data;
  }, []);

  ///get Users data
  const getGroupsData = useCallback(async () => {
    let response = await getGroups();
    console.log();
    setGroups(response.data);
    return response.data;
  }, []);

  const setSeedData = async () => {
    await getGroupsData();
  };

  useEffect(() => {
    if (history.location?.state?.id) {
      console.log(history.location?.state);
      setEditData(history.location?.state);
    }
    setSeedData();
  }, []);

  //dropdown onchange
  const dropDownOnchange = (eve, name, value, setFieldValue) => {
    console.log(eve, name, value, setFieldValue);
    if (name === "groupId") {
      console.log("ineer");
      getSubGroupData(eve.target.value);
    }
    setFieldValue(name, eve.target.value);
  };

  //get subform id
  const getEditId = (id) => {
    console.log(id);
    let editValue = { ...formikRef.current.values["person"][id] };
    console.log(editValue);
    setsubFormEditIndex(id);
    formikRef.current.setFieldValue("personData", editValue);
  };

  //set active form status
  const changeActiveForm = (type) => {
    if (type === "main") {
      if (formikRef.current.values["subFormActive"] === true) {
        formikRef.current.setErrors({});
        formikRef.current.setTouched({});
      }
      formikRef.current.setFieldValue("subFormActive", false);
    } else {
      if (formikRef.current.values["subFormActive"] === false) {
        formikRef.current.setErrors({});
        formikRef.current.setTouched({});
      }

      formikRef.current.setFieldValue("subFormActive", true);
    }
  };

  return (
    <div>
      <div>
        <div>
          <Grid container>
            <Grid item xs={1}></Grid>
            <Grid item xs={8} className={styles.formHead}>
              <FormHeader
                path={EmsPath.MASTERSCONVERSATIONList}
                name="Conversatin"
              />
            </Grid>
          </Grid>
          <Grid container className={styles.product}>
            <Grid item xs={1}></Grid>

            <Formik
              initialValues={{ ...conversationState }}
              validationSchema={conversationValidationSchema}
              innerRef={formikRef}
              onSubmit={(value, r) => {
                formSubmit(value, r);
              }}
            >
              <>
                <Grid item xs={10}>
                  <Form onClick={(e) => changeActiveForm("main")}>
                    <ConversationMainForm
                      groups={groups}
                      subGroups={subGroups}
                      dropDownOnchange={dropDownOnchange}
                      mainFormAudioFileUploadKeys={mainFormAudioFileUploadKeys}
                    />
                  </Form>
                  <Form onClick={(e) => changeActiveForm("sub")}>
                    <CommonSubForm
                      COLUMNS={conversationSubListColomn}
                      FormCancel={FormCancel}
                      Schema={conversationSubFormSchema}
                      subFormEditIndex={subFormEditIndex}
                      getEditId={getEditId}
                      errorName="person"
                      subFormHeaderText="Conversation"
                      formKey="subFormActive"
                    />
                  </Form>
                </Grid>
                <Grid item xs={12}>
                  <FormFooter updateText={history.location?.state?.id} formKey="subFormActive" FormCancel={FormCancel} />
                </Grid>
              </>
            </Formik>
          </Grid>
        </div>
      </div>
    </div>
  );
}

export default ConversationForm;
