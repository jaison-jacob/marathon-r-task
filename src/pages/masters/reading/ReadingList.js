import { Grid } from "@material-ui/core";
import React, { useState, useEffect,useCallback } from "react";
import { Link, useHistory } from "react-router-dom";
import { EmsPath } from "../../../router/EmsPath";
import { CommonTable } from "../../../component/reusables";
import { readingColomn } from "../../../constants/reading/ReadingTableSchema";
import AddIcon from '@material-ui/icons/Add';
import Nodata from "../../../component/reusables/Nodata";
import {getReading} from "../../../api/Api"
import "react_custom_table/src/styles/index.scss";

function ReadingList(props) {
  let history = useHistory();
  // let Styles = useStyles();

  const [readingTable, setreadingTable] = useState({
    list: [],
    pagination: {
      pageSize: 10,
      currentPage: 1,
    },
    selection:[],
    count: 0,
    search: "",
    filterData: {},
    currentSortIndex: null,
    isSorted: false,
    showPopup: false,
  });

  /**pagination handle  */
  const getList = useCallback(async (pageNumber, size) => {
    console.log(pageNumber, size);
    try {
      await getReading(size, (pageNumber - 1) * size).then((res) => {
        const { data } = res;
        console.log(data);
        if (data?.count > 0) {
          setreadingTable((prevState) => ({
            ...prevState,
            count: data.count,
            list: [...data.rows],
            pagination: { currentPage: pageNumber, pageSize: size },
          }));
        }
      });
    } catch (error) {
      console.log(error);
    }
  }, []);

  //get subList data from redux
  useEffect(() => {
    getList(
      readingTable.pagination.currentPage,
      readingTable.pagination.pageSize
    );
  }, []);

  const getEditId = async (index,type) => {
    let state = { id: index,type:type };
    await history.push(EmsPath.MASTERSREADINGFORM, state);
  };

  //filter data based on search
  const searchFilter = (val) => {
    let copyState = { ...readingTable };
    let newList = copyState.list.filter((o) =>
      Object.keys(o).some((k) => String(o[k]).includes(val))
    );
    setreadingTable({ ...copyState, list: [...newList] });
  };

  const onPageNumberChange = (page) => {
    getList(page, readingTable.pagination.pageSize);
  };
  const onChangePageSize = (newPageSize) => {
    getList(1, newPageSize);
  };

  return (
    <Grid container style={{padding:"20px 30px"}}>
      {readingTable.list.length === 0 ? (
        <Grid item xs={12}>
          <Nodata
          text="Reading"
          btnText="New story"
          path={EmsPath.MASTERSREADINGList}
          />
        </Grid>
      ) : (
        <Grid item xs={12}>
          <CommonTable
            columns={readingColomn(getEditId)}
            data={readingTable.list}
            selection={[]}
            LoadingComponent={() => <div></div>}
            tableControlProps={{
              tableTitle: "Reading",
              search: true,
              filter: false,
              addNew: true,
              download: false,
              addButtonProps: {
                label: "New story",
                leftIcon: <div></div>,
                rightIcon: <div></div>,
                onClick: () => history.push(EmsPath.MASTERSREADINGFORM),
              }
            }}
            totalCount={readingTable.count}
            pageSize={readingTable.pagination.pageSize}
            currentPage={readingTable.pagination.currentPage}
            onPageNumberChange={onPageNumberChange}
            onChangePageSize={onChangePageSize}
            showPagination={true}
          />
        </Grid>
      )}
    </Grid>
  );
}


export default ReadingList;