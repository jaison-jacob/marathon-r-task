import React, { useState, useEffect, useRef, useCallback } from "react";
import { Grid} from "@material-ui/core";
import {
  FormHeader,
  FormFooter,
} from "../../../component/reusables";
import { Formik, Form} from "formik";

import {
  readingState,
  mainFormImageFileUploadKeys,
  conversationSubFormSchema,
} from "../../../constants/reading/Reading";
import { readingSubListColomn } from "../../../constants/reading/ReadingTableSchema";
import ReadingForm from "../../../component/masters/reading";
import { readingValidationSchema } from "../../../validation/masters/Reading";
import { useStyles } from "../../styles/CommonFormContainer";
import { useHistory } from "react-router-dom";
import { EmsPath } from "../../../router/EmsPath";
import CommonSubForm from "../../../component/masters/CommonSubForm";
import {
  getGroups,
  getSubGroupsById,
  createReading,
  getReadingById,
  updateReading,
  getLanguage,
  getKeywords,
} from "../../../api/Api";

function Reading(props) {
  let history = useHistory();
  const styles = useStyles();
  const formikRef = useRef();
  const [groups, setGroups] = useState([]);
  const [subGroups, setSubGroups] = useState([]);
  const [language, setLanguage] = useState([]);
  const [keywords, setkeywords] = useState([]);
  const [subFormEditIndex, setsubFormEditIndex] = useState(null);

  //set edit value to feilds
  const setEditData = async (state) => {
    const { id, type } = state;
    console.log(id);
    let editTranslationData = await getReadingById(id);
    editTranslationData = { ...editTranslationData.data };
    console.log(editTranslationData);
    await getGroupsData();
    await getSubGroupData(editTranslationData["groupId"]);
    let obj = {};
    Object.keys(editTranslationData).map((e) => {
      if (readingState.hasOwnProperty(e)) {
        obj[e] = editTranslationData[e];
      }
    });
    formikRef.current.setValues({
      ...obj,
      keywords: getKeyWordFromState(obj["keywords"]),
    });
  };

  const getKeyWordFromState = (sourceArray) => {
    let getArray = [];
    for (let i in sourceArray) {
      for (let j in keywords) {
          console.log(keywords[j].id,sourceArray[i].storyWordId)
        if (keywords[j].id === sourceArray[i].storyWordId) {
          getArray.push(keywords[j]);
        }
      }
    }
    console.log(getArray);
    return getArray;
  };

  //click cancel of Form this function is trigger
  const FormCancel = (type) => {
    if (type === "subForm") {
      let copyquestioniresData = { ...readingState["questioniresData"] };
      formikRef.current.setFieldValue("questioniresData", copyquestioniresData);
    } else {
      history.push(EmsPath.MASTERSREADINGList, {});
    }
  };

  //form submition
  const formSubmit = (value, r) => {
    console.log("in ite submit");
    if (value["subFormActive"] === true) {
      let copyTranslation = [...value["questionires"]];
      let copyquestioniresData = { ...readingState["questioniresData"] };
      formikRef.current.setTouched({});
      formikRef.current.setSubmitting(false);

      if (subFormEditIndex === null) {
        copyTranslation.push(value["questioniresData"]);
      } else {
        copyTranslation[subFormEditIndex] = { ...value["questioniresData"] };
        setsubFormEditIndex(null);
      }

      formikRef.current.setFieldValue("questionires", copyTranslation);
      formikRef.current.setFieldValue("questioniresData", copyquestioniresData);
    } else {
      let finalData = {
        ...value,
        keywords: value["keywords"].map((item) => item.id),
      };
      console.log(finalData);
      if (history.location?.state?.id) {
        console.log("IN", finalData);
        updateReading(finalData, history.location?.state?.id)
          .then((res) => {
            console.log(res);
            history.push(EmsPath.MASTERSREADINGList, {});
          })
          .catch((e) => {
            console.log(e);
          });
      } else {
        createReading(finalData)
          .then((res) => {
            console.log(res);
            history.push(EmsPath.MASTERSREADINGList, {});
          })
          .catch((e) => {
            console.log(e);
          });
      }
    }
  };

  ///get subGroup data
  const getSubGroupData = useCallback(async (id) => {
    let response = await getSubGroupsById(id);
    setSubGroups(response.data);
    return response.data;
  }, []);

  ///get Groups data
  const getGroupsData = useCallback(async () => {
    let response = await getGroups();
    setGroups(response.data);
    return response.data;
  }, []);

  ///get keywords data
  const getkeywordsData = useCallback(async () => {
    let response = await getKeywords();

    setkeywords(response.data.rows);
    return response.data.rows;
  }, []);

  ///get language data
  const getLanguageData = useCallback(async () => {
    let response = await getLanguage();
    setLanguage(response.data);
    return response.data;
  }, []);

  const setSeedData = async () => {
    await getGroupsData();
    await getLanguageData();
    await getkeywordsData();
  };

  useEffect(() => {
    if (history.location?.state?.id) {
      console.log("hello");
      setEditData(history.location?.state);
    }
    setSeedData();
  }, []);

 

  //dropdown onchange
  const dropDownOnchange = (eve, name, value, setFieldValue) => {
    if (name === "groupId") {
      getSubGroupData(eve.target.value);
    }
    setFieldValue(name, eve.target.value);
  };

  //radio onchange
  const radioOnchange = (eve, name, value, setFieldValue) => {
    setFieldValue(name, Number(eve.target.value));
  };

  //get subform id
  const getEditId = (id, type) => {
    let editValue = [...formikRef.current.values["questionires"]];
    if (type === "edit") {
      formikRef.current.setFieldValue("questioniresData", editValue[id]);
      setsubFormEditIndex(id);
    } else {
      let c = editValue.filter((item, index) => index !== id);
      formikRef.current.setFieldValue("questionires", [...c]);
    }
  };

  //set active form status
  const changeActiveForm = (type) => {
    if (type === "main") {
      if (formikRef.current.values["subFormActive"] === true) {
        formikRef.current.setErrors({});
        formikRef.current.setTouched({});
      }
      formikRef.current.setFieldValue("subFormActive", false);
    } else {
      if (formikRef.current.values["subFormActive"] === false) {
        formikRef.current.setErrors({});
        formikRef.current.setTouched({});
      }

      formikRef.current.setFieldValue("subFormActive", true);
    }
  };

  return (
    <div>
      <div>
        <div>
          <Grid container>
            <Grid item xs={1}></Grid>
            <Grid item xs={8} className={styles.formHead}>
              <FormHeader
                path={EmsPath.MASTERSTRANSLATELIST}
                name="Translate"
              />
            </Grid>
          </Grid>
          <Grid container className={styles.product}>
            <Grid item xs={1}></Grid>

            <Formik
              initialValues={{ ...readingState }}
              validationSchema={readingValidationSchema}
              innerRef={formikRef}
              onSubmit={(value, r) => {
                formSubmit(value, r);
              }}
            >
              <>
                <Grid item xs={10}>
                  <Form onClick={(e) => changeActiveForm("main")}>
                    <ReadingForm
                      groups={groups}
                      subGroups={subGroups}
                      dropDownOnchange={dropDownOnchange}
                      mainFormAudioFileUploadKeys={mainFormImageFileUploadKeys}
                      keywords={keywords}
                      language={language}
                      radioOnchange={radioOnchange}
                    />
                  </Form>
                  <Form onClick={(e) => changeActiveForm("sub")}>
                    <CommonSubForm
                      COLUMNS={readingSubListColomn}
                      FormCancel={FormCancel}
                      Schema={conversationSubFormSchema}
                      subFormEditIndex={subFormEditIndex}
                      getEditId={getEditId}
                      errorName="questionires"
                      subFormHeaderText="Reading"
                      formKey="subFormActive"
                    />
                  </Form>
                </Grid>
                <Grid item xs={12}>
                  <FormFooter updateText={history.location?.state?.id} formKey="subFormActive" FormCancel={FormCancel} />
                </Grid>
              </>
            </Formik>
          </Grid>
        </div>
      </div>
    </div>
  );
}

export default Reading;