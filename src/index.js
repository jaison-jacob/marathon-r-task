import React from "react"
import ReactDOM from "react-dom"
import reportWebVitals from "./reportWebVitals"
import "./style/TableStyle.scss"
import { Provider } from "react-redux"
import { BrowserRouter as Router, Route,Switch } from "react-router-dom"
import { store, history } from "./Store"
import Root from "./pages/Root"
import Login from "./pages/front/login"

ReactDOM.render(
	<Provider store={store}>
		<Router history={history}>
		<Switch>
    <Route path="/" component={Login} exact={true} />
    <Route path="/home" component={Root} exact={false} />
	<Root/>
    </Switch>
		</Router>
	</Provider>,
	document.getElementById("root")
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()