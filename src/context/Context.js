import {createContext} from "react"

const context = createContext()

const ContectProvider = context.Provider

export {ContectProvider,context}