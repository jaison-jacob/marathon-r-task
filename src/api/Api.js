import axios from "axios";
import { Urls } from "./Urls";
import {store} from "../Store";
import { appLoading } from "../store/actions/App";

const {dispatch} = store
export const appApi = axios.create({
  baseURL: "http://localhost:4000/",
  headers: {
    Accept: "application/json",
    "content-type": "application/json",
  },
});

//interceptors

appApi.interceptors.request.use(
  (config) => {
    config.headers["authorization"] = `Bearer abc`;
    dispatch(appLoading(true));

    return config;
  },
  (error) => {
    dispatch(appLoading(false));
    return Promise.reject(error);
  }
);

appApi.interceptors.response.use(
  (response) => {
    dispatch(appLoading(false));

    return response;
  },
  (error) => {
    const { data } = error.response ? error.response : {};
console.log(data)
    dispatch(appLoading(false));
    // if (error?.response?.status === 403) {
    //   // if unauthorized
    //   dispatch(
    //     showSnackbarMessage({
    //       severity: SNACKBAR_SEVERITY.error,
    //       message: "Unauthorized! Please Login again.",
    //     })
    //   );
    // } else {
    //   dispatch(
    //     showSnackbarMessage({
    //       severity: SNACKBAR_SEVERITY.error,
    //       message: data?.message || data?.error || "Server Error",
    //     })
    //   );
    // }
    return Promise.reject(error);
  }
);




//seeds
export const getCompanyType = () => appApi.get(Urls.GET_COMPANY_TYPE);
export const getCountry = () => appApi.get(Urls.GET_COUNTRY);
export const getState = () => appApi.get(Urls.GET_STATES);
export const getDistrictById = (id) =>
  appApi.get(Urls.GET_DISTRICT_BYSTATEID + `${id}`);
export const getLanguage = () => appApi.get(Urls.GET_LANGUAGE);
export const getGender = () => appApi.get(Urls.GET_GENDER);
export const getUsers = () => appApi.get(Urls.GET_USERS);
export const getFileType = () => appApi.get(Urls.GET_FILE_TYPE);
export const getAllCompany = () => appApi.get(Urls.GET_ALL_COMPANY);
export const getGroups = () => appApi.get(Urls.GET_GROUP_AS_SEED);
export const getSubGroupsById = (id) =>
  appApi.get(Urls.GET_SUBGROUPS + `${id}`);
export const getKeywords = () => appApi.get(Urls.GET_KEYWORDS);

//company
export const createCompany = (data) => appApi.post(Urls.CREATE_COMPANY, data);
export const getCompany = (size, pageDataFrom) =>
  appApi.get(Urls.GET_COMPANY + `${size}/${pageDataFrom}`);
export const getCompanyById = (id) =>
  appApi.get(Urls.GET_COMPANYBYID + `${id}`);
export const updateCompany = (data, id) =>
  appApi.put(Urls.UPADATE_COMPANY + `${id}`, data);

//campaignPlan
export const createCampaignPLan = (data) =>
  appApi.post(Urls.CREATE_CAMPAIGN_PLAN, data);

export const getCampaignPlan = (size, pageDataFrom) =>
  appApi.get(Urls.GET_CAMPAIGN_PLAN + `${size}/${pageDataFrom}`);

export const getCampaignPlanById = (id) =>
  appApi.get(Urls.GET_CAMPAIGN_PLAN_BYID + `${id}`);

export const updateCampaignPLan = (data, id) =>
  appApi.put(Urls.UPDATE_CAMPAIGN_PLAN + `${id}`, data);

//conversation
export const createConversation = (data) =>
  appApi.post(Urls.CREATE_CONVERSATION, data);

export const getConversation = (size, pageDataFrom) =>
  appApi.get(Urls.GET_CONVERSATION + `${size}/${pageDataFrom}`);

export const getConversationById = (id) =>
  appApi.get(Urls.GET_CONVERSATION_BY_ID + `${id}`);

export const updateConversation = (data, id) =>
  appApi.put(Urls.UPDATE_CONVERSATION + `${id}`, data);

//translation
export const createTranslation = (data) =>
  appApi.post(Urls.CREATE_TRANSLATION, data);
export const updateTranslation = (data, id) =>
  appApi.put(Urls.UPDATE_TRANSLATION + `${id}`, data);
export const getTranslationById = (id) =>
  appApi.get(Urls.GET_TRANSLATION_BY_ID + `${id}`);

export const getTranslation = (size, pageDataFrom) =>
  appApi.get(Urls.GET_TRANSLATION + `${size}/${pageDataFrom}`);

//reading
export const createReading = (data) => appApi.post(Urls.CREATE_READING, data);
export const updateReading = (data, id) =>
  appApi.put(Urls.UPDATE_READING + `${id}`, data);
export const getReadingById = (id) =>
  appApi.get(Urls.GET_READING_BY_ID + `${id}`);
export const getReading = (size, pageDataFrom) =>
  appApi.get(Urls.GET_READINGS + `${size}/${pageDataFrom}`);
