export const Urls = {
  //seeds
  GET_COMPANY_TYPE: "seeds/CompanyTypes",
  GET_COUNTRY: "seeds/Country",
  GET_STATES: "seeds/States",
  GET_DISTRICT_BYSTATEID: "districtsByStateId/",
  GET_LANGUAGE: "seeds/Languages",
  GET_GENDER: "gender",
  GET_USERS: "users",
  GET_FILE_TYPE: "seeds/FileTypes",
  GET_ALL_COMPANY: "company",
  GET_GROUP_AS_SEED: "groupAsSeeds",
  GET_SUBGROUPS: "subGroupByGroupId/",
  GET_KEYWORDS: "storyWord",

  //company
  CREATE_COMPANY: "company",
  GET_COMPANY: "company/",
  GET_COMPANYBYID: "companyById/",
  UPADATE_COMPANY: "company/",

  //campaignPlan
  CREATE_CAMPAIGN_PLAN: "campaignPlan",
  GET_CAMPAIGN_PLAN: "campaignPlan/",
  GET_CAMPAIGN_PLAN_BYID: "campaignPlanById/",
  UPDATE_CAMPAIGN_PLAN: "campaignPlan/",

  //conversation
  CREATE_CONVERSATION: "createConversation",
  GET_CONVERSATION: "getConversation/",
  GET_CONVERSATION_BY_ID: "getConversationById/",
  UPDATE_CONVERSATION: "updateConversation/",

  //translation
  CREATE_TRANSLATION: "createTranslate",
  UPDATE_TRANSLATION: "updateTranslate/",
  GET_TRANSLATION_BY_ID: "getTranslateById/",
  GET_TRANSLATION: "getTranslate/",

  //reading
  CREATE_READING: "reading",
  UPDATE_READING: "updateReading/",
  GET_READING_BY_ID: "getReadingById/",
  GET_READINGS: "readings/",
};
