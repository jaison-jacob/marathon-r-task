import CampaignPlanForm from "../pages/adCampaign/campaignPlan/CampaignPlanForm"
import CampaignPlanList from "../pages/adCampaign/campaignPlan/CampaignPlanList"
import CompanyForm from "../pages/adCampaign/company/CompanyForm"
import CompanyList from "../pages/adCampaign/company/CompanyList"
import ConversationForm from "../pages/masters/conversation/ConversationForm"
import ConversationList from "../pages/masters/conversation/ConversationList"
import TranslateForm from "../pages/masters/translate/TranslateForm"
import TranslateList from "../pages/masters/translate/TranslateList"
import ReadingForm from "../pages/masters/reading/ReadingForm"
import ReadingList from "../pages/masters/reading/ReadingList"
import Login from "../pages/front/login"

import {EmsPath} from "./EmsPath"


export const routes = [
    {
        path: EmsPath.LOGIN,
		component: Login,
		exact: true,
    },
    {
		path: EmsPath.MASTERSTRANSLATELIST,
		component: TranslateList,
		exact: false,
	},
    {
		path: EmsPath.MASTERSCONVERSATIONFORM,
		component: ConversationForm,
		exact: false,
	},
	{
		path: EmsPath.MASTERSCONVERSATIONList,
		component: ConversationList,
		exact: false,
	},
	{
		path: EmsPath.MASTERSTRANSLATEFORM,
		component: TranslateForm,
		exact: false,
	},
    {
		path: EmsPath.MASTERSREADINGFORM,
		component: ReadingForm,
		exact: false,
	},
    {
		path: EmsPath.MASTERSREADINGList,
		component: ReadingList,
		exact: false,
	},
    {
		path: EmsPath.ADCAMPAIGNCAMPAIGNPLANFORM,
		component: CampaignPlanForm,
		exact: false,
	},
    {
		path: EmsPath.ADCAMPAIGNCAMPAIGNPLANLIST,
		component: CampaignPlanList,
		exact: false,
	},
    {
		path: EmsPath.ADCAMPAIGNCOMPANYFORM,
		component: CompanyForm,
		exact: false,
	},
    {
		path: EmsPath.ADCAMPAIGNCOMPANYLIST,
		component: CompanyList,
		exact: false,
	}
]