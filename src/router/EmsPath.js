export const EmsPath = {
    LOGIN:"/",
    MASTERSCONVERSATIONFORM:"/home/master/conversationForm",
    MASTERSCONVERSATIONList:"/home/master/conversationlist",
    MASTERSREADINGFORM:"/home/master/ReadingForm",
    MASTERSREADINGList:"/home/master/ReadingList",
    MASTERSTRANSLATEFORM:"/home/master/TranslateForm",
    MASTERSTRANSLATELIST:"/home/master/TranslateList",
    ADCAMPAIGNCOMPANYFORM:"/home/adcampaign/CompanyForm",
    ADCAMPAIGNCOMPANYLIST:"/home/adcampaign/CompanyList",
    ADCAMPAIGNCAMPAIGNPLANFORM:"/home/adcampaign/CampaignPlanForm",
    ADCAMPAIGNCAMPAIGNPLANLIST:"/home/adcampaign/CampaignPLanList"
}