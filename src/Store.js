import rootReducer from "./store/reducers";
import { createStore, compose, applyMiddleware } from "redux"
import { routerMiddleware } from "react-router-redux"
import { createBrowserHistory } from "history"
import { createLogger } from "redux-logger"
import thunk from "redux-thunk"

export const history = createBrowserHistory()

let middleware = [thunk, routerMiddleware(history)]
middleware.push(createLogger())

const composemiddleware = compose(applyMiddleware(...middleware))
export const store = createStore(rootReducer, composemiddleware)