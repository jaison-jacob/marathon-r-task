import * as Yup from "yup";

export const conversationSubForm = Yup.object().shape({
    firstPersonText:Yup.string().required("required"),
    secPersonText:Yup.string().required("required"),
    firstPersonAudioName:Yup.array().required("please pick atlest one"),
    secPersonAudioName:Yup.array().required("please pick atlest one"),
})


export const conversationMainForm = Yup.object().shape({
    name:Yup.string().min(5,"Please enter charecter 5").required("required"),
    groupId:Yup.array().required("please pick atlest one"),
    subGroupId:Yup.array().required("please pick atlest one"),
    person:Yup.array().test({
        name: "person_test",
        exclusive: true,
        message: "At least select one",
        test: (value) => value.length > 0,
      })
})

export const  translateState ={
	"name": "Translate1",
	"englishText": "englishText",
	"englishAudio": "englishAudio",
	"englishAudioName": "englishAudioName",
	"englishAudioType": "englishAudioType",
	"englishAudioAwsName": "englishAudioAwsName",
	"groupId": 1,
  subGroupId:null,
	"translationDetails": [],
  translateData:{
    "sourceLangId": null,
    "sourceText": "",
    "sourceAudio": "",
    "sourceAudioName": "",
    "sourceAwsAudioName": "",
    "sourceAudioType": ""
  },
  subFormActive:false
}


export const mainFormValidation = Yup.object().shape({
  name: Yup.string().required("conversation name is required"),
  
  audioName: Yup.string().required("Audio is required"),
  
  groupId: Yup.string().required("group is required").nullable(true),

  subGroupId: Yup.string().required("subGroup is required").nullable(true),
  person: Yup.array().test({
        name: "person_test",
        exclusive: true,
        message: "please add one conversation",
        test: (value) => value.length > 0,
      }),
  
})

export const subFormValidation = Yup.object().shape({
  personData:Yup.object({
    firstPersonText:Yup.string().required("required"),
    firstPersonAudioName:Yup.string().required("required"),
    secPersonText:Yup.string().required("required"),
    secPersonAudioName:Yup.string().required("required"),
})
})

export const conversationValidationSchema = Yup.object().shape({
  subFormActive:Yup.boolean(),
  name:Yup.mixed().when("subFormActive", {
      is: false,
      then: Yup.string().required("conversation name is required"),
    }),
    audioName:Yup.mixed().when("subFormActive", {
      is: false,
      then: Yup.string().required("Audio is required"),
    }),
    groupId:Yup.mixed().when("subFormActive", {
      is: false,
      then: Yup.string().required("group is required").nullable(true),
    }),
    subGroupId:Yup.mixed().when("subFormActive", {
      is: false,
      then: Yup.string().required("subGroup is required").nullable(true),
    }),


    personData:Yup.mixed().when("subFormActive", {
      is: true,
      then: Yup.object({
          firstPersonText:Yup.string().required("required"),
          firstPersonAudioName:Yup.string().required("required"),
          secPersonText:Yup.string().required("required"),
          secPersonAudioName:Yup.string().required("required"),
      })
    }),
    person:Yup.mixed().when("subFormActive", {
          is: false,
          then: Yup.array().test({
              name: "person_test",
              exclusive: true,
              message: "please add one conversation",
              test: (value) => value.length > 0,
            }),
        }),

  
    
})
