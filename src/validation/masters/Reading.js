import * as Yup from "yup";

export const conversationSubForm = Yup.object().shape({
    firstPersonText:Yup.string().required("required"),
    secPersonText:Yup.string().required("required"),
    firstPersonAudioName:Yup.array().required("please pick atlest one"),
    secPersonAudioName:Yup.array().required("please pick atlest one"),
})


export const conversationMainForm = Yup.object().shape({
    name:Yup.string().min(5,"Please enter charecter 5").required("required"),
    groupId:Yup.array().required("please pick atlest one"),
    subGroupId:Yup.array().required("please pick atlest one"),
    person:Yup.array().test({
        name: "person_test",
        exclusive: true,
        message: "At least select one",
        test: (value) => value.length > 0,
      })
})

export const readingState = {
    "baseStoryName":"", 
      "baseStoryLang":null,
     "baseStoryImage":"",
     "imagePosition":1,
     "baseStoryText":"",
     "groupId":null,
     "subGroupId":null,
     "questionires":[],
  questioniresData:{
    "question": "sdfs02_Question",
        "questionAudio": "sdfs02_Question",
        "questionAudioName": "sdfs02_Question",
        "questionAwsAudioName": "sdfs02_Question",
        "questionAudioType": "sdfs02_Question",
    "answerText": "sa01_answer",
        "answerAudio": "sa01_answer",
        "answerAudioName": "sa01_answer",
        "answerAwsAudioName": "sa01_answer",
        "answerAudioType": "sa01_answer"
  },
     "keywords": [],
     subFormActive:false,
  }


export const mainFormValidation = Yup.object().shape({
    baseStoryName: Yup.string().required("story name is required"),
  
    baseStoryImage: Yup.string().required("Image is required"),
  baseStoryLang:Yup.string().required("group is required").nullable(true),
  baseStoryText:Yup.string().min(10,"please text about story above 10 charecters").required("about story text is required"),
  groupId: Yup.string().required("group is required").nullable(true),

  subGroupId: Yup.string().required("subGroup is required").nullable(true),
  questionires: Yup.array().test({
        name: "person_test",
        exclusive: true,
        message: "please add one conversation",
        test: (value) => value.length > 0,
      }),
  
})

export const subFormValidation = Yup.object().shape({
  personData:Yup.object({
    firstPersonText:Yup.string().required("required"),
    firstPersonAudioName:Yup.string().required("required"),
    secPersonText:Yup.string().required("required"),
    secPersonAudioName:Yup.string().required("required"),
})
})

export const readingValidationSchema = Yup.object().shape({
  
  subFormActive:Yup.boolean(),
  baseStoryName:Yup.mixed().when("subFormActive", {
      is: false,
      then: Yup.string().required("story name is required"),
    }),
    baseStoryImage:Yup.mixed().when("subFormActive", {
      is: false,
      then: Yup.string().required("Image is required"),
    }),
    baseStoryLang:Yup.mixed().when("subFormActive", {
        is: false,
        then: Yup.string().required("group is required").nullable(true),
      }),
      baseStoryText:Yup.mixed().when("subFormActive", {
        is: false,
        then: Yup.string().min(10,"please text about story above 10 charecters").required("about story text is required"),
      }),
    groupId:Yup.mixed().when("subFormActive", {
      is: false,
      then: Yup.string().required("group is required").nullable(true),
    }),
    subGroupId:Yup.mixed().when("subFormActive", {
      is: false,
      then: Yup.string().required("subGroup is required").nullable(true),
    }),
    questionires:Yup.mixed().when("subFormActive", {
        is: false,
        then: Yup.array().test({
            name: "questionires_test",
            exclusive: true,
            message: "please add one conversation",
            test: (value) => value.length > 0,
          }),
      }),

    questioniresData:Yup.mixed().when("subFormActive", {
      is: true,
      then: Yup.object({
       
        question:Yup.string().required("required"),
        questionAudioName:Yup.string().required("required"),
          answerText:Yup.string().required("required"),
          answerAudioName:Yup.string().required("required"),
      })
    }),
    keywords:Yup.mixed().when("subFormActive", {
          is: false,
          then: Yup.array().test({
              name: "questionires_test",
              exclusive: true,
              message: "please select one keyword",
              test: (value) => value.length > 0,
            }),
        }),

  
    
})
