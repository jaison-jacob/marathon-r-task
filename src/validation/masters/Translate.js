import * as Yup from "yup";

export const conversationSubForm = Yup.object().shape({
    firstPersonText:Yup.string().required("required"),
    secPersonText:Yup.string().required("required"),
    firstPersonAudioName:Yup.array().required("please pick atlest one"),
    secPersonAudioName:Yup.array().required("please pick atlest one"),
})


export const conversationMainForm = Yup.object().shape({
    name:Yup.string().min(5,"Please enter charecter 5").required("required"),
    groupId:Yup.array().required("please pick atlest one"),
    subGroupId:Yup.array().required("please pick atlest one"),
    person:Yup.array().test({
        name: "person_test",
        exclusive: true,
        message: "At least select one",
        test: (value) => value.length > 0,
      })
})

export const conversationState = {
	"name" : "",
	"audioName": "",
	"audioAwsName": "",
	"audioType": "",
	"audioUrl": "",
  "groupId": null,
	"subGroupId": null,
	"person": [
	],
    "persaonData":{
        "firstPersonText": "text1",
        "firstPersonAudioPath": "audioUrl",
        "firstPersonAudioName": "audio1",
        "firstPersonAwsAudioName": "audio1",
        "firstPersonAudioType": "mimeType",
        "secPersonText": "text2",
        "secPersonAudioPath": "audioUrl",
        "secPersonAudioName": "audio2",
        "secPersonAwsAudioName": "audio1",
        "secPersonAudioType": "mimeType"
    },
    subFormActive:false,

}

export const translationValidationSchema = Yup.object().shape({
    subFormActive:Yup.boolean(),
    name:Yup.mixed().when("subFormActive", {
        is: false,
        then: Yup.string().required("translation name is required"),
      }),
      englishText:Yup.mixed().when("subFormActive", {
        is: false,
        then: Yup.string().required("english language text is required"),
      }),
      englishAudioName:Yup.mixed().when("subFormActive", {
        is: false,
        then: Yup.string().required("Audio is required"),
      }),
      groupId:Yup.mixed().when("subFormActive", {
        is: false,
        then: Yup.string().required("group is required").nullable(true),
      }),
      subGroupId:Yup.mixed().when("subFormActive", {
        is: false,
        then: Yup.string().required("subGroup is required").nullable(true),
      }),


      translateData:Yup.mixed().when("subFormActive", {
        is: true,
        then: Yup.object({
          sourceText:Yup.string().required("required"),
          sourceAudioName:Yup.string().required("required"),
          sourceLangId:Yup.string().required("subGroup is required").nullable(true)
        })
      }),
      translationDetails:Yup.mixed().when("subFormActive", {
            is: false,
            then: Yup.array().test({
                name: "translationDetails_test",
                exclusive: true,
                message: "please add one translation",
                test: (value) => value.length > 0,
              }),
          }),

    
      
})

