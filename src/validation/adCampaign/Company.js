import * as Yup from "yup";
import { number } from "yup";

export const companyState = {
	"name": "",
	"companyTypeId": 1,
	"companyAddress": "",
	"countryId": 1,
	"stateName": null,
	"districtName": null,
	"userName": "",
	"userDesignation": "",
	"userEmail": "",
	"userMobile": "",
	"pan":"",
	"gst":"",
    "cityText":""
}

export const companyFormValidationSchema = Yup.object().shape({
    name:Yup.string().required("required"),
    companyTypeId:number().required("required").nullable(true),
    stateName:Yup.mixed().when("countryId", {
        is: 1,
        then: Yup.number().required("state is required").nullable(true),
      }),
      districtName:Yup.mixed().when("countryId", {
        is: 1,
        then: Yup.number().required("city is required").nullable(true),
      }),
      cityText:Yup.mixed().when("countryId", {
        is: 2,
        then: Yup.string().required("city is required"),
      }),
      userName:Yup.string().min(5,"please text minimum 5 charecter").required("required"),
      userEmail:Yup.string().email("please enter valid email").required("required"),
      userMobile:Yup.string().min(10,"enter valid phone number").max(10,"please enter valid phone number").required("required")
})