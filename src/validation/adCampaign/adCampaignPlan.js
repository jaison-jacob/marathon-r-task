import * as Yup from "yup";
import { number } from "yup";

export const adCampaignPlanState = {
	"name": "Campaign1",
	"companyId": 2,
	"startDate": null,
	"endDate": null,
	"startTime": "",
	"endTime": "",
	"languageId": 1,
	"countryId": 2,
	"countryName":"",
	"stateName": "Tamilnadu",
	"districtName": "Tirunelveli",
	"genderId": 2,
	"customAdDisplay": true,
	"customCampaignType": 1,
	"fileName": "file01",
	"fileAwsName": "aws",
	"fileUrl": "aws",
	"allUsers": true,
	"users": [1]
}

export const adCampaignPlanFormValidationSchema = Yup.object().shape({
    name:Yup.string().required("required").min(5,"please enter min 5 charecter"),
    companyId:number().required("required").nullable(true),
    stateName:Yup.mixed().when("countryId", {
        is: 1,
        then: Yup.number().required("state is required").nullable(true),
      }),
      districtName:Yup.mixed().when("countryId", {
        is: 1,
        then: Yup.number().required("city is required").nullable(true),
      }),
      cityText:Yup.mixed().when("countryId", {
        is: 2,
        then: Yup.string().required("city is required"),
      }),
      countryName:Yup.mixed().when("countryId", {
        is: 2,
        then: Yup.string().required("country is required"),
      }),
      fileName:Yup.string().required("required"),
      users:Yup.array().required("required")
})