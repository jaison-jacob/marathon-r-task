import { combineReducers } from "redux"
import { adCampaignPlanReducer} from "./actions/adCampaignPlan"
import {adCampaignCompanyReducer} from "./actions/company"
import {mastersConversationReducer} from "./actions/conversation"
import {mastersListeningReducer} from "./actions/listening"
import {mastersReadingReducer} from "./actions/reading"

export const reducers = combineReducers({
	adCampaignPlanReducer,adCampaignCompanyReducer,mastersConversationReducer,
    mastersListeningReducer,mastersReadingReducer
})