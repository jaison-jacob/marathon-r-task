export const groupName = [
    {
        id:1,
        name:"Junior"
    },
    {
        id:2,
        name:"senior"
    },
    {
        id:3,
        name:"superSenior"
    },
]

export const selectSubGroup = [
    {
        id:1,
        name:"Male"
    },
    {
        id:2,
        name:"Female"
    },
    {
        id:3,
        name:"Others"
    },
]